import cs2321.*;


public class Main {
        public static void main(String[] args){
                DynamicStack<String> stack = new DynamicStack<String>();

                stack.push("Go!");
                stack.push("... ");
                stack.push("1 ");
                stack.push("2 ");
                stack.push("3 ");

                while(stack.isEmpty()==false){
                        System.out.print(stack.pop());
                }
        }
}
