package cs2321;

import java.util.Iterator;
import java.util.NoSuchElementException;

import net.datastructures.*;
import cs2321.*;

/**
 * A stack that uses a dynamic amount of memory. If the capacity is exceeded
 * the capacity will be doubled. 
 * 
 * An array is used for storage with O(n) space complexity
 * 
 * DynamicStack(), size(), top(), isEmpty() all have a Time Complexity cost of 1
 * toString() has a Space and Time Complexity cost of n
 * pop() has a Time Complexity Amortized cost of 1
 * push() has a Space and Time complexity of n, but a Time Complexity Amortized cost of 1
 * 
 * Course: CS2321 Section R01
 * Assignment: #1
 * @author Andrew Markiewicz - AJMARKIE
 */ 


public class DynamicStack<E> implements Stack<E>,Iterable<E> {

	private E[] data;      // Data held by the data structure
	int size;      // current number of items in the stack
	private int cap;	   // The current capacity of the stack




	/**
	 * Create an empty stack.
	 */
	@TimeComplexity("O(1)")
	// TCJ Only one operation is called
	public DynamicStack() {
		data = (E[])new Object[1];
		size = 0;
		cap = 0;
	}


	/**
	 * Returns the number of components in the stack.
	 *
	 * @return  the number of components in the stack.
	 * 
	 * @see Stack#size()
	 */
	@TimeComplexity("O(1)")
	// TCJ Only one operation is called
	public int size() {
		return size;
	}


	/**
	 * Tests if this stack is empty.
	 * 
	 * @return  <code>true</code> if and only if this stack contains 
	 *          no items; <code>false</code> otherwise.
	 *          
	 * @see Stack#isEmpty()
	 */
	@TimeComplexity("O(1)")
	// TCJ Only one operation is called
	public boolean isEmpty() {

		return size==0;
	}

	/**
	 * Pushes an item onto the top of the stack.
	 *
	 * @param   newEntry   the item to be pushed onto the stack.
	 * 
	 * @see Stack#push(E)
	 */
	@TimeComplexity("O(n)")
	@SpaceComplexity("O(n)")
	// SCJ The space that now is used is based on the amount of data (n) that is inserted to the stack
	@TimeComplexityAmortized("O(1)")
	// TCJ Since the array is doubled only when the capacity of the array is reached, the Time Complexity averages to 1
	public void push(E newEntry){

		//So wont be stuck in 0*0 loop
		if(size==0)
			cap = 1;

		// Increase the size of storage and allocate new storage
		size++;

		int i =0;

		//Double the capacity if it is reached
		if(size > cap) {
			cap = cap * 2;

			//Creates the new array that is doubled in size
			E[] temp = (E[])(new Object[cap]);

			// New element is placed at the end position of the stack
			temp[size-1] = newEntry;


			// TCJ: Copy/loop of size n is O(n)
			// Remaining elements are added onto array
			for(E e : data){    
				temp[i] = e;
				i++;
			}

			// Replace the field's value with the new storage
			data = temp;
		}

		else{ 
			//If the size is less than the capacity
			//Enter the new Node at the size index
			data[size-1] = newEntry;
		}

	} // End void push(E)

	/**
	 * Returns the top of the stack w/o removing it
	 * @return The top element of the stack
	 * @throws EmptyStackException Throws exception if the stack is empty
	 */
	@TimeComplexity("O(1)")
	// TCJ Only one operation is called
	public E top() throws EmptyStackException {
		if(size==0)
			throw new EmptyStackException("Empty Stack");
		return data[size -1];
	}

	/**
	 * Removes and returns the top of the stack
	 * @return The top element of the stack
	 * @throws EmptyStackException Throws exception if the stack is empty
	 */
	@TimeComplexity("O(1)")
	// TCJ One operation called
	public E pop() throws EmptyStackException {
		if(size==0)
			throw new EmptyStackException("Empty Stack");

		E temp = top();
		
		size--;
		return temp;
	}

	/**
	 * Returns a string of  all the elements in the stack
	 * @return String containing all elements of the stack
	 */
	@TimeComplexity("O(n)")
	@SpaceComplexity("O(n)")
	// TCJ The entire stack must be read through and placed into a string
	// SCJ All items in the stack must be ammended to the string
	public String toString(){

		String toReturn = "[";

		//Goes through the array 'backwards' or the stack forwards
		for(int i = size; i>0; i--){

			toReturn += data[i-1];
			if(i>1)
				toReturn += ", ";
		}	
		toReturn += "]";
		return toReturn;
	}

	/**
	 * An iterator class for the Dynamic Stack
	 * @author Andrew Markiewicz - AJMARKIE
	 *
	 */
	@SpaceComplexity("O(n)")
	// SCJ Makes a copy of the entire stack of n elements
	private class Itr implements Iterator<E>{
		// The current "cursor" indicates the next element in the sequence
		private int cursor=0;
		private E[] dataCopy;	

		/**
		 * Constructor used to copy the current stack
		 */
		@TimeComplexity("O(n)")
		public Itr() {
			// Copy all the data in the data structure into a new temp array
			// TCJ: Allocation and loop/copy of size n are each O(n)
			// SCJ: Space complexity is O(n) due to this allocation
			dataCopy = (E[])new Object[size];
			for(int i=0;i<size;i++) {
				dataCopy[i]=data[i];
			}
		}

		/**
		 * Used to check if there is another element in the stack
		 */
		@TimeComplexity("O(1)")
		public boolean hasNext() {   
			return cursor<size;  
		}

		/**
		 * Returns the next element in the stack
		 */
		@TimeComplexity("O(1)")
		public E next() { 
			if(cursor>=size)
				throw new NoSuchElementException();
			// Advance the cursor
			cursor++;
			// Return the preceding element
			return dataCopy[cursor-1];
		}

		@TimeComplexity("O(1)")
		public void remove() {
			throw new UnsupportedOperationException();
		}	

	}

	/**
	 * Creates and returns a new iterator Object
	 */
	@TimeComplexity("O(1)")
	public Iterator<E> iterator() {
		return new Itr();
	}

	/**
	 * Main method used to test the Dynamic Stack
	 * @param args
	 */
	@TimeComplexity("O(?)")   
	@SpaceComplexity("O(?)") 
	public static void main(String [] args) {
		DynamicStack<Integer> stack = new DynamicStack<Integer>();
		if(!stack.toString().equals("[]")) throw new RuntimeException("toString() Failure");


		if(!stack.isEmpty()) throw new RuntimeException("isEmpty() Failure");

		stack.push(1);
		if(!(stack.isEmpty()==false)) throw new RuntimeException("isEmpty() Failure");

		if(!(stack.size()==1)) throw new RuntimeException("size() Failure");

		if(!(stack.top()==1)) throw new RuntimeException("top() Failure");


		stack.push(2);
		if(!(stack.isEmpty()==false)) throw new RuntimeException("isEmpty() Failure");

		if(!(stack.size()==2)) throw new RuntimeException("size() Failure");

		if(!(stack.top()==2)) throw new RuntimeException("top() Failure");


		if(!(stack.toString().equals("[2, 1]"))) throw new RuntimeException("toString() Failure");


		System.out.println("Testing iterator (for-each to print all elements):");
		for(Integer i:stack) {
			System.out.println("  " + i);
		}

		System.out.println("All tests completed");

		// Measure some insertion times. Take repeated samples to find average time.
		final int SAMPLES=5000;         // Number of operations to test

		Stack<Integer> [] stacks = new DynamicStack[SAMPLES];
		for(int i=0;i<SAMPLES;i++) {
			stacks[i] = new DynamicStack<Integer>();
		}
		// Insert N elements into each stack
		final int N=100;
		System.out.println("  n  | time (ms)");
		System.out.println("----------------");

		// This loop will print the average time taken to insert an element into
		// an empty stack, followed by the average time taken to insert a second
		// element into a stack, followed by the average time taken to insert a 
		// third element, etc. Up to the average time taken to insert the Nth element. 
		for(int i=0;i<N;i++) {
			long startTime = System.currentTimeMillis();
			// Do a push for each sample stack
			for(int j=0;j<SAMPLES; j++) {
				stacks[j].push(i);
			}
			long stopTime = System.currentTimeMillis();
			System.out.printf("%4d , %7.5f %n",i+1,(stopTime-startTime)/(double)SAMPLES);
		}
	} // End void main(String [])

}
