package cs2321;

import java.util.Iterator;
import java.util.NoSuchElementException;
import net.datastructures.*;
import cs2321.*;

/*#
 * This file contains several styles of comment: 
 *
 *    Special CS2321 comments - comments that begin with "/*#" (Like this comment block)
 *         Comments in this style are used to indicate special features we want to see 
 *         in your source code for cs2321. They merely highlight what we are looking for. 
 *         Search for /*# and you can easily see all the "special features" you should
 *         have in your assignments as well as some notes about techniques we will use 
 *         in class.
 *         
 *         You should not use these comments in your own code. 
 *         And if you use this source code as a starting point for your assignment, 
 *         please remove all the comments that begin with this special marker.        
 *
 *    Documentation (javadoc) Comments - comments that begin with "/**" 
 *        These comments will allow someone to understand and use the data structure with minimum effort 
 *        by simply reading the generated javadoc documentation.
 *        
 *    Implementation Comments - comments that begin with "//" or "/*"
 *        These comments act as guides to explain what is being accomplished in the code.
 *        They clarify why it is doing what it is, they do NOT just re-state what the code says.
 * 
 *    Complexity Comments - comments that begin with either "// TCJ:", "/* TCJ:", "// SCJ:", or "/* SCJ:"
 *        
 *         TCJ - Time Complexity Justification (TCJ). 
 *               These comments explain to the grader/world why you have claimed the 
 *               time complexity you have. Use "TCJ" so the grader can clearly find 
 *               these comments and separate them from normal implementation comments.
 *               
 *         SCJ - Space Complexity Justification (SCJ) 
 *               These comments explain to the grader/world why you have claimed the 
 *               space complexity you have. Use "SCJ" so the grader can clearly find 
 *               these comments and separate them from normal implementation comments.
 * 
 * This file provides an example of how CS2321 data structures should be implemented.
 * Things to note: 
 *   1. The data structure declaration has an annotation indicating its space complexity @SpaceComplexity
 *   2. EVERY method's declaration has an annotation indicating its time complexity via @TimeComplexity
 *   3. Methods with a significant additional space complexity, also indicate the additional space required.
 *   4. Pay attention to the Iterator (& how it works). It allows the "for-each" iteration through the structure.
 * 
 */

/*# Note the initial block that gives a basic description of the class */
/**
 * A stack that uses the minimum amount of memory possible.
 * 
 * An array is used for storage (O(n) space complexity)
 * push(), pop(), iterator(), and toString() are O(n) time and space complexity.
 * top(), size(), and isEmpty() are O(1) time and space complexity.
 * 
 * Note: Operations that effect the stack's size (push & pop) require a total of 2*N memory temporarily
 *       Iterators and toString create objects with O(n) space.
 * 
 * Course: CS2321 Section ALL
 * Assignment: #1
 * @author Chris Brown (cdbrown@mtu.edu)
 * @author Bill Siever (bsiever@mtu.edu)
 */
@SpaceComplexity("O(n)")            /*# See Note #1 */
public class MinSpaceStack<E> implements Stack<E>,Iterable<E> {

	private E[] data;      // Data held by the data structure
	private int size;      // current number of items in the stack

	/**
	 * Create an empty stack.
	 */
	@SuppressWarnings("unchecked")
	@TimeComplexity("O(1)") /*# See Note #2 */
	public MinSpaceStack() {
		data = (E[])new Object[0];
		size = 0;
	}

	/**
	 * Returns the number of components in the stack.
	 *
	 * @return  the number of components in the stack.
	 * 
	 * @see Stack#size()
	 */
	@TimeComplexity("O(1)") /*# See Note #2 */
	public int size() {
		return size;
	}

	/**
	 * Tests if this stack is empty.
	 * 
	 * @return  <code>true</code> if and only if this stack contains 
	 *          no items; <code>false</code> otherwise.
	 *          
	 * @see Stack#isEmpty()
	 */
	@TimeComplexity("O(1)") /*# See Note #2 */
	public boolean isEmpty() {
		return size==0;
	}

	/** 
	 * Removes and returns the object at the top of the stack.
	 * 
	 * @return     The object at the top of the stack.
	 * 
	 * @exception  EmptyStackException  if this stack is empty.
	 * 
	 * @see Stack#pop()
	 */
	@SuppressWarnings("unchecked")
	@TimeComplexity("O(n)")  /*# See Note #2 */
	@SpaceComplexity("O(n)") /*# See Note #3 */
	public E pop() throws EmptyStackException {
		E ret = top();

		// Decrease size of storage and allocate new storage
		size--;		

		/*# Notice the complexity comments: */
		// TCJ: Allocation of size n is O(n)
		E[] temp = (E[])(new Object[size]); 

		// TCJ: Loop/copy of size n is O(n)
		// Remove first / shift everything else left one location
		for(int i = 0; i < size; i++){
			temp[i] = data[i+1];       
		}

		// Replace the field's value with the new storage
		data = temp;

		return ret;
	} // End E pop()

	/**
	 * Pushes an item onto the top of the stack.
	 *
	 * @param   newEntry   the item to be pushed onto the stack.
	 * 
	 * @see Stack#push(E)
	 */
	@SuppressWarnings("unchecked")
	@TimeComplexity("O(n)")  /*# See Note #2 */
	@SpaceComplexity("O(n)") /*# See Note #3 */
	public void push(E newEntry){
		int i = 0;

		// Increase the size of storage and allocate new storage
		size++;

		// TCJ: Allocation of size n is O(n)
		// SCJ: Space complexity is O(n) due to this allocation:
		E[] temp = (E[])(new Object[size]); 
		// New element becomes 0th element in array
		temp[0] = newEntry;
		i++;

		// TCJ: Copy/loop of size n is O(n)
		// Remaining elements are added onto array
		for(E e : data){       /*# An example of "for-each" syntax */
			temp[i] = e;
			i++;
		}

		// Replace the field's value with the new storage
		data = temp;
	} // End void push(E)

	/**
	 * Returns the object at the top of this stack without removing it 
	 * from the stack. 
	 *
	 * @return     the object at the top of this stack 
	 * 
	 * @exception  EmptyStackException  if this stack is empty.

	 * @see Stack#top()
	 */
	@TimeComplexity("O(1)") /*# See Note #2 */
	public E top() throws EmptyStackException {
		if(size==0)
			throw new EmptyStackException("Empty Stack");
		return data[0];
	}

	/*
	 * toString is often useful when debugging and testing. 
	 * 
	 * @see java.lang.Object#toString()
	 */
	@TimeComplexity("O(n)")  /*# See Note #2 */
	@SpaceComplexity("O(n)") /*# See Note #3 */
	public String toString() {

		/* TCJ: Construction of string for n items is >= O(n)
		 *      (Assuming simple, fixed-size items O(n))
		 */
		/* SCJ: Space complexity is >=O(n) due to concatenation to this String
		 *      (Assuming simple, fixed-size items O(n))
		 */
		String ret = "[";
		for(int i=0;i<size;i++) {
			if(i>0)
				ret += ", ";
			ret += data[i];
		}

		ret += "]";
		return ret;
	} // End String toString()	


	// An iterator to traverse through elements.
	/*# Note:
	 * 
	 *  Iterators "iterate" through data. They are often used in data structures
	 *  to provide a simple way to look at all the data that the data structure
	 *  contains. 
	 *   
	 *  Java has a special for loop notation (called the for-each) that can
	 *  be used for arrays or any classes that implement the "Iterable" interface.
	 * "Iterable" things just have a method named "iterator()" that creates
	 *  an Iterator. So, any programmer can use this special syntax to 
	 *  process all the data in their data structure.
	 * 
	 *  Unfortunately iterators can lead to some logic flaws. In particular what 
	 *  happens if someone is "iterating through" all the data in a data structure, 
	 *  pauses, inserts an element, and continues iterating. There are three
	 *  approaches to consider:
	 *  
	 *  1) The iterator should only "iterate through" the data that was in the data
	 *     structure when the iterator was created, so it won't see the new data.
	 *     This requires that the iterator create a private copy of all the data in the
	 *     data structure when the iterator is first created. It will iterate through
	 *     this private copy and no longer have any ties to the data structure.
	 *     
	 *     This is the approach taken here and by the authors of net.datastructures. 
	 *     The copy of the data requires more space than is required by the other two 
	 *     approaches.
	 *     
	 *  2) The iterator should become invalid and throw an exception if it's used
	 *     after the data structure changes.
	 *     
	 *     This is the approach taken by the data structures in Java's java.util.*.
	 *     It requires less space, but "changes" must be monitored and used to 
	 *     invalidate the iterator. The basic techniques for this are simple and
	 *     efficient, but add extra logic to any method that may modify the 
	 *     data structure.
	 *     
	 *   3) The iterator could ignore changes to the data structure.
	 *    
	 *      This approach is simpler to implement and requires less space, 
	 *      but could lead to some very difficult-to-detect logic errors.
	 *      
	 */
	@SpaceComplexity("O(n)") /*# See Note #1 */
	private class Itr implements Iterator<E> {
		// The current "cursor" indicates the next element in the sequence
		private int cursor=0;
		private E[] dataCopy;

		@SuppressWarnings("unchecked")
		@TimeComplexity("O(n)")  /*# See Note #2 */
		public Itr() {
			// Copy all the data in the data structure.
			// (A simple clone would be sufficient here, 
			//  but not for many later data structures)

			// TCJ: Allocation and loop/copy of size n are each O(n)
			// SCJ: Space complexity is O(n) due to this allocation
			dataCopy = (E[])new Object[size];
			for(int i=0;i<size;i++) {
				dataCopy[i]=data[i];
			}
		} // End Itr()

		@TimeComplexity("O(1)")  /*# See Note #2 */
		public boolean hasNext() {   
			return cursor<size;  
		}

		@TimeComplexity("O(1)") /*# See Note #2 */
		public E next() { 
			if(cursor>=size)
				throw new NoSuchElementException();
			// Advance the cursor
			cursor++;
			// Return the preceding element
			return dataCopy[cursor-1];
		}

		// Iterators in cs2321 don't need to support removal.
		@TimeComplexity("O(1)")  /*# See Note #2 */
		public void remove() {
			throw new UnsupportedOperationException();
		}

	} // End class Itr

	/**
	 * Returns an iterator over the elements in the stack.
	 * 
	 * @return an iterator over the elements in the stack.
	 */
	@TimeComplexity("O(1)") /*# See Note #2 */
	public Iterator<E> iterator() {
		return new Itr();
	}



	/**
	 * Test driver - test basic operations of the minimum space stack.
	 */
	@SuppressWarnings("unchecked")
	@TimeComplexity("O(?)")    /*# See Note #2 */ 
	@SpaceComplexity("O(?)")   /*# See Note #3 */
	static public void main(String [] args) {

		final int SAMPLES = 5000;

		//	Stack<Integer> [] stacks = new MinSpaceStack[SAMPLES];

		 System.out.println("----------");
		final int N=200;        
		for(int i=0;i<N;i++) {
			long startTime = System.currentTimeMillis();
			for(int j=0; j<SAMPLES; j++){
				Stack<Integer> temp = new MinSpaceStack<Integer>();
				for(int k=0; k<i;k++){
					temp.push(i);
				}
			}
			long stopTime = System.currentTimeMillis();
			
			System.out.printf("%7.5f %n",(stopTime-startTime)/(double)SAMPLES);
		}
	}			

} // End void main(String [])


