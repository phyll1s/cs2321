import cs2321.*;

/**
 * A test driver for MergeSort, QuickSort, and RadixSort.
 * 
 * Course: CS2321 Section ALL
 * Assignment: #6
 * @author Chris Brown (cdbrown@mtu.edu) and Andrew Markiewicz (AJMARKIE)
 */

public class Main {

	public static void main(String [] args){
		java.util.Random random = new java.util.Random();

		int x = 8000;

		//Random Data
		Integer[] array1= new Integer[x];
		for(int i=0; i<x;i++){
			array1[i]=random.nextInt(100000);
		}
		Integer[] array2 = array1.clone();
		Integer[] array3 = array1.clone();

		//Sorted Data
		Integer[] array4 = new Integer[x];
		for(int i=0; i<x; i++){
			array4[i] = i;
		}
		Integer[] array5 = array4.clone();
		Integer[] array6 = array4.clone();

		//Reverse Sorted Data
		Integer[] array7 = new Integer[x];
		for(int i=0; i<x; i++){
			array7[i] = x-i;
		}
		Integer[] array8 = array7.clone();
		Integer[] array9 = array7.clone();

		//Large Random Data
		Integer[] array10 = new Integer[x];
		for(int i=0; i<x;i++){
			array10[i]=random.nextInt(1000000000);
		}
		Integer[] array11 = array10.clone();
		Integer[] array12 = array10.clone();

		System.out.println("Random Data");
		System.out.println("-----------");
		//System.out.println(testMergeSort(array1, 500));
		System.out.println(testQuickSort(array2, 500));
		//System.out.println(testRadixSort(array3, 500));

		System.out.println("Sorted Data");
		System.out.println("-----------");
		//System.out.println(testMergeSort(array4, 500));
		System.out.println(testQuickSort(array5, 500));
		//System.out.println(testRadixSort(array6, 500));

		System.out.println("Reverse Sorted Data");
		System.out.println("-----------");
		//System.out.println(testMergeSort(array7, 500));
		System.out.println(testQuickSort(array8, 500));
		//System.out.println(testRadixSort(array9, 500));

		System.out.println("Large Random Data");
		System.out.println("-----------");
		//System.out.println(testMergeSort(array10, 500));
		System.out.println(testQuickSort(array11, 500));
		//System.out.println(testRadixSort(array12, 500));



	}

	/**
	 * Algorithm: testMergeSort
	 * @param arr - an array of Comparables to use for empirical measurement of merge sort.
	 * @param minTime - the minimum amount of time to run tests
	 * @return average time taken
	 */
	private static int testMergeSort(Comparable[] arr, int minTime){

		MergeSort<Integer> mS = new MergeSort<Integer>();
		System.gc();
		int samples=0;
		long stopTime;
		long startTime = System.currentTimeMillis();
		do {
			mS.sort((Integer[]) arr);  // Sort the data
			samples++;
			stopTime = System.currentTimeMillis();
		} while(stopTime-startTime < minTime);
		double averageTime = ((stopTime-startTime)/(1.0*samples));

		return (int) averageTime;	
	}

	/**
	 * Algorithm: testQuickSort
	 * @param arr - an array of Comparables to use for empirical measurement of quick sort.
	 * @param minTime - the minimum amount of time to run tests
	 * @return average time taken
	 */
	private static int testQuickSort(Comparable[] arr, int minTime){

		QuickSort<Integer> qS = new QuickSort<Integer>();
		System.gc();
		int samples=0;
		long stopTime;
		long startTime = System.currentTimeMillis();
		do {
			qS.sort((Integer[]) arr);  // Sort the data
			samples++;
			stopTime = System.currentTimeMillis();
		} while(stopTime-startTime < minTime);
		double averageTime = ((stopTime-startTime)/(1.0*samples));

		return (int) averageTime;		
	}

	/**
	 * Algorithm: testRadixSort
	 * @param arr - an array of Integers to use for empirical measurement of Radix sort.
	 * @param minTime - the minimum amount of time to run tests
	 * @return average time taken
	 */
	private static int testRadixSort(Integer[] arr, int minTime){

		RadixSort rS = new RadixSort();
		System.gc();
		int samples=0;
		long stopTime;
		long startTime = System.currentTimeMillis();
		do {
			rS.sort((Integer[]) arr);  // Sort the data
			samples++;
			stopTime = System.currentTimeMillis();
		} while(stopTime-startTime < minTime);
		double averageTime = ((stopTime-startTime)/(1.0*samples));

		return (int) averageTime;

	}
}
