package cs2321;

import java.util.Iterator;
import java.util.NoSuchElementException;
import net.datastructures.*;

/**
 * A Linked node based Sequence
 * 
 * Course: CS2321 Section R01
 * Assignment: #3
 * @author AJMARKIE - Andrew Markiewicz
 * 
 * This sequence implements the Sequence interface. 
 * A doubly linked list is used for storage and has a SpaceComplexity of O(n)
 * //SCJ The sequence uses one 'space' for every piece of data
 * 
 * It's methods that have O(1) time complexity are:
 * Constructor()
 * addFirst()
 * addLast()
 * getFirst()
 * getLast()
 * isEmpty()
 * removeFirst()
 * removeLast()
 * size()
 * first()
 * last()
 * iterator()
 * 
 * It's methods that have O(n) time complexity are:
 * 
 * atIndex()
 * indexOf()
 * find()
 * ifExists()
 * add()
 * get()
 * remove()
 * set()
 * addAfter()
 * addBefore()
 * next()
 * positions()
 * prev()
 * remove()
 * set()
 * toString()
 * 
 * It's methods that have O(n) spacecomplexity are:
 * 
 * positions()
 * 
 */
@SpaceComplexity("O(n)")
//SCJ The sequence uses one 'space' for every piece of data
public class LinkedSequence<E> implements Sequence<E> {

	/**
	 * Node class used to hold an element and contain 'links' to other elements
	 * @author AJMARKIE - Andrew Markiewicz
	 *
	 * @param <E> Type of data to be stored
	 */
	@TimeComplexity("O(1)")
	@SpaceComplexity("O(1)")
	//TCJ Only one operation is called
	//SCJ One one piece of data is stored
	private class Node implements Position<E>{

		Node previous;
		Node next;
		E element;		

		@TimeComplexity("O(1)")
		//TCJ Only one operation is called		
		public Node(E e, Node p, Node n){

			element = e;
			next = n;
			previous = p;			
		}		

		/**
		 * Sets the previous element of the node
		 * @param p The node to be set as previous
		 */
		@TimeComplexity("O(1)")
		//TCJ Only one function is called
		public void setPrevious(Node p){
			previous = p;			
		}

		/**
		 * Sets the next element of the node
		 * @param n The node to be set as the next
		 */
		@TimeComplexity("O()")
		//TCJ Only one operation is called
		public void setNext(Node n){
			next = n;
		}

		/**
		 * Returns the previous Node
		 * @return The node previous to the one being called
		 */
		@TimeComplexity("O(1)")
		//TCJ Only one operation is called
		public Node getPrevious(){
			return previous;
		}

		/**
		 * Returns the next Node
		 * @return The next node
		 */
		@TimeComplexity("O(1)")
		//TCJ Only one operation is called
		public Node getNext(){
			return next;
		}


		/**
		 * Returns the element
		 * @return the element that the node holds
		 */
		@TimeComplexity("O(1)")
		//TCJ Only one operation is called
		public E element() {
			return element;
		}

		/**
		 * Method to store a new element
		 * @param newE The new element to store
		 */
		@TimeComplexity("O(1)")
		//TCJ Only one operation is called
		public void setElement(E newE){
			element = newE;
		}

		@TimeComplexity("O(1)")
		//TCJ Only one operation is called
		public String toString(){
			String toReturn =""+element;
			return toReturn;			
		}


	}

	//Creates head and tail sentinal nodes and size counter
	private Node head;
	private Node tail;
	private int size;

	/**
	 * Constructor for the linked sequence class
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public LinkedSequence() {

		head = new Node(null, null, null);
		tail = new Node(null, null, null);
		head.setNext(tail);
		tail.setPrevious(head);
		size = 0;

	}

	/**
	 * Returns the Position at the index specified
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate through the sequence until it gets to the given position
	public Position<E> atIndex(int r) throws BoundaryViolationException {

		if(r>=size() || isEmpty() || r<0)
			throw new BoundaryViolationException("Out of bounds index");

		Node toReturn = head.getNext();
		for(int i=0; i<r; i++)
			toReturn = toReturn.getNext();

		return toReturn;
	}

	/**
	 * Returns the index of the position
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate through the sequence until it gets to the given position
	public int indexOf(Position<E> p) throws InvalidPositionException {

		int index = 0;
		for(int i=0; i<size(); i++){
			if(atIndex(i) == p){
				break;
			}			 
			index ++;
		}
		if(!ifExists(p))
			throw new InvalidPositionException("This position doesn't exist in the sequence");
		return index;
	}

	/**
	 * Adds an element to the front of the sequence
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public void addFirst(E element) {

		Node toAdd = new Node(element, head, head.getNext());
		head.getNext().setPrevious(toAdd);
		head.setNext(toAdd);
		size++;

	}

	/**
	 * Adds an element to the end of the sequence
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public void addLast(E element) {

		Node toAdd = new Node(element, tail.getPrevious(), tail);
		tail.getPrevious().setNext(toAdd);
		tail.setPrevious(toAdd);
		size++;

	}

	/**
	 * Returns the first element
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public E getFirst() throws EmptyDequeException {

		if(isEmpty())
			throw new EmptyDequeException("Empty Sequence");

		return this.first().element();
	}

	/**
	 * Returns the last element
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public E getLast() throws EmptyDequeException {

		if(isEmpty())
			throw new EmptyDequeException("Empty Sequence");
		return this.last().element();
	}

	/**
	 * Checks to see if the sequence is empty
	 * @return Boolean - True if the sequence is empty, false otherwise 
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public boolean isEmpty() {

		return (size==0);
	}

	/**
	 * Removes the first element and returns it
	 * @return the first element
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called 
	public E removeFirst() throws EmptyDequeException {

		if(isEmpty())
			throw new EmptyDequeException("Empty Deque");

		Node toRemove = head.getNext();
		head.getNext().getNext().setPrevious(head);
		head.setNext(toRemove.getNext());
		size--;
		return toRemove.element();
	}

	/**
	 * Removes the last element and returns it
	 * @return the last element in the sequence
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public E removeLast() throws EmptyDequeException {

		if(isEmpty())
			throw new EmptyDequeException("Empty Deque");
		Node toRemove = tail.getPrevious();
		tail.getPrevious().getPrevious().setNext(tail);
		tail.setPrevious(toRemove.getPrevious());
		size--;
		return toRemove.element();
	}

	/**
	 * Gets the current size of the sequence
	 * @return the current size of the sequence
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public int size() {

		return size;
	}

	/**
	 * Method used to find a position within a sequence
	 * @param p The position to be located
	 * @return The position
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate through the sequence to the given position (worst case n)
	public Node find(Position<E> p){

		Node current = head.getNext();
		for(int i=0; i<size; i++){
			if(current == p){

				break;				
			}
			current = current.getNext();
		}

		return current;
	}

	/**
	 * Method to find a position and determine if it exists in the sequence
	 * @param p The position to be found
	 * @return The position
	 */
	@TimeComplexity("O(n)")
	//TCJ THe method must iterate through the sequence t othe given position (worst case n)
	public boolean ifExists(Position<E> p){


		Node current = head.getNext();
		boolean exists = false;
		for(int i=0; i<size; i++){
			if(current == p){
				exists = true;
				break;				
			}
			current = current.getNext();
		}
		if(current == p){
			exists = true;
		}

		return exists;
	}

	/**
	 * Insert element into sequence at position i, shifting the rest over
	 */
	@TimeComplexity("O(n)")
	//TCJ The method has to iterate through the sequence thice ( O(n) )
	public void add(int i, E e) throws IndexOutOfBoundsException {

		if(isEmpty() || i>size() || i<0)
			throw new IndexOutOfBoundsException("This position is not in this sequence");

		Node current = head;
		for(int j=0; j<i; j++){
			current = current.getNext();
		}
		Node toAdd = new Node(e, current, current.getNext());
		current.getNext().setPrevious(toAdd);
		current.setNext(toAdd);
		size++;
	}

	/**
	 * Returns the element at position i
	 */
	@TimeComplexity("O(n)")
	//TCJ The method has to iterate through the sequence thice ( O(n) )
	public E get(int i) throws IndexOutOfBoundsException {

		if(isEmpty() || i>=size() || i<0)
			throw new IndexOutOfBoundsException("This position is not in this sequence");

		Node current = head.getNext();
		for(int j=0; j<i; j++){
			current = current.getNext();
		}
		return current.element();
	}

	/**
	 * Removes the element at the position given
	 */
	@TimeComplexity("O(n)")
	//TCJ The method has to iterate through the sequence thice ( O(n) )	
	public E remove(int i) throws IndexOutOfBoundsException {

		if(isEmpty() || i>size() || i<0)
			throw new IndexOutOfBoundsException("This position is not in this sequence");

		Node current = head.getNext();
		for(int j=0; j<i; j++){
			current = current.getNext();
		}
		current.getPrevious().setNext(current.getNext());
		current.getNext().setPrevious(current.getPrevious());
		size--;

		return current.element();
	}

	/**
	 * Sets the element at i to a new element and returns the old
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate through the sequence until the given index
	public E set(int i, E e) throws IndexOutOfBoundsException {

		if(isEmpty() || i>size() || i<0)
			throw new IndexOutOfBoundsException("This position is not in this sequence");

		Node current = head.getNext();
		for(int j=0; j<i; j++){
			current = current.getNext();
		}
		E toReturn = current.element();
		current.setElement(e);
		return toReturn;
	}

	/**
	 * Adds a position after the position specified
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate the sequence until it gets to the position specified
	public void addAfter(Position<E> p, E e) throws InvalidPositionException {

		try{		

			Node current= (Node) p;

			Node toAdd = new Node(e, current, current.getNext());
			current.getNext().setPrevious(toAdd);
			current.setNext(toAdd);
			size++;
		}catch(NullPointerException exception){
			throw new InvalidPositionException("Position does not exist in sequence");
		}

	}

	/**
	 * Adds a position before the position specified
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate the sequence until it gets to the position specified
	public void addBefore(Position<E> p, E e) throws InvalidPositionException {
		try{


			Node current = (Node) p;

			Node toAdd = new Node(e, current.getPrevious(), current);
			current.getPrevious().setNext(toAdd);
			current.setPrevious(toAdd);
			size++;
		}catch(NullPointerException exception){
			throw new InvalidPositionException("Position does not exist in sequence");
		}

	}

	/**
	 * Returns the first element in the sequence without removing it
	 * @return the first element in the sequence
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public Position<E> first() {

		if(isEmpty())
			throw new EmptyListException("Empty Sequence");
		return head.getNext();
	}

	/**
	 * Returns the last element in the sequence without removing it
	 * @return the last element in the sequence
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public Position<E> last() {

		if(isEmpty())
			throw new EmptyListException("Empty Sequence");

		return tail.getPrevious();
	}

	/**
	 * Returns an iterator that iterates through the entire sequence
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public Iterator<E> iterator() {

		Itr iterator = new Itr<E>();
		return iterator;
	}

	/**
	 * Returns the position after the position given
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate through the sequence until it gets to the given position
	public Position<E> next(Position<E> p) throws InvalidPositionException,
	BoundaryViolationException {

		try{
			Node current = (Node) p;
			if(current.getNext() == tail)
				throw new BoundaryViolationException("The next postiion is out of bounds");
			return current.getNext();
		}catch(NullPointerException exception){
			throw new InvalidPositionException("Position does not exist in sequence");
		}
	}

	/**
	 * Returns an iterable collection of all the nodes in the list.
	 */
	@TimeComplexity("O(n)")
	@SpaceComplexity("O(n)")
	//TCJ The method must iterate through the entire sequence to copy it
	//SCJ The method creates a new sequence that takes up n size
	public Iterable<Position<E>> positions() {

		Sequence<Position<E>> positionList = new LinkedSequence<Position<E>>();

		Node current = head.getNext();
		for(int i=0; i<size; i++){
			positionList.addLast(current);
			current = current.getNext();

		}		
		return positionList;
	}

	/**
	 * Returns the position before the position given
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate through the sequence until it gets to the given position
	public Position<E> prev(Position<E> p) throws InvalidPositionException,	BoundaryViolationException {

		try{

			Node current = (Node) p;

			if(current.getPrevious()==head)
				throw new BoundaryViolationException("The previous postiion is out of bounds");
			return current.getPrevious();
		}catch(NullPointerException exception){
			throw new InvalidPositionException("Position does not exist in sequence");
		}
	}

	/**
	 * Removes the position and returns the element it held
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate through the sequence until it gets to the given position
	public E remove(Position<E> p) throws InvalidPositionException {

		try{

			Node current = (Node) p;
			current.getPrevious().setNext(current.getNext());
			current.getNext().setPrevious(current.getPrevious());
			size--;
			return current.element();

		}catch(NullPointerException exception){
			throw new InvalidPositionException("Position does not exist in sequence");
		}
	}

	/**
	 * Sets the element of the position given to a new element and returns the old one
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate through the sequence until it gets to the given position
	public E set(Position<E> p, E e) throws InvalidPositionException {

		try{

			Node current = (Node) p;		
			E toReturn = p.element();
			current.setElement(e);		
			return toReturn;
		}catch(NullPointerException exception){
			throw new InvalidPositionException("Position does not exist in sequence");
		}
	}

	/**
	 * Iterator class that iterates through every element in a sequence
	 * @author AJMARKIEWICZ - Andrew Markiewicz
	 *
	 * @param <E> Generic Type
	 */
	@SpaceComplexity ( "O(1)")
	//SCJ The method holds and returns one element at a time
	private class Itr<E> implements java.util.Iterator<E>{

		Node current = head;

		@TimeComplexity ("O(1)")
		//TCJ Method only called one operation
		public boolean hasNext() {

			if(current.getNext() != tail)
				return true;
			else
				return false;
		}
		@TimeComplexity ("O(1)")
		//TCJ Method only called one operation
		public E next() {

			try{				
				current = current.getNext();
			} catch(NoSuchElementException e) {
				return null;
			}
			return (E) current.element();
		}

		public void remove() {
			throw new UnsupportedOperationException("Did not implement");
		}
	}

	/**
	 * Method to return a string that contains every element in the sequence
	 */
	@TimeComplexity("O(n)")
	//TCJ The method must iterate through the entire sequence
	public String toString(){

		String toReturn = "[";
		int index =0;
		Iterable<Position<E>> copy = positions();

		for(Position<E> n: copy){
			toReturn += n.element();
			index++;
			if(index< size)
				toReturn += ", ";
		}
		toReturn += "]";

		return toReturn;

	}

}
