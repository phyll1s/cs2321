package cs2321;

/**
 * A merge sort built using an array
 * @author AJMARKIE - Andrew Markiewicz
 *
 * @param <E> The generic type that is stored in the array
 */
@SpaceComplexity ("O(log(n)")
//SCJ There will need to be multiple arrays that hold the same elements
public class MergeSort<E extends Comparable> implements Sorter<E> {

	/**
	 * Sorts the array passed in
	 */
	@TimeComplexity ("O(n * log(n))")
	//TCJ All the elements need to be iterated through multiple times
	public void sort(E[] array) {
		E[] temp = sorts(array);
		for(int i=0; i<array.length; i++){
			array[i]=temp[i];
		}
	}

	/**
	 * Does a merge sort on the array
	 * @param array The array to merge sort
	 * @return The sorted array
	 */
	public E[] sorts(E[] array){
		int n = array.length;

		//Base Case
		if(n<2){
			return array;
		}

		int sizeLeft, sizeRight;

		//Checks for arrays w/ odd # of elements
		if(n%2 == 1){
			sizeLeft= n/2 +1;
			sizeRight=n/2;
		} else {
			sizeLeft=n/2;
			sizeRight=n/2;
		}

		E[] leftArray = (E[]) new Comparable[sizeLeft];
		E[] rightArray = (E[]) new Comparable[sizeRight];

		//Splits the elements into two arrays
		for(int i=0; i<sizeLeft; i++){
			leftArray[i]=array[i];			
		}
		for(int i=0; i<sizeRight; i++){
			rightArray[i]=array[i+(sizeLeft)];						
		}
		return merge(sorts(leftArray), sorts(rightArray));
	}

	/**
	 * Merges two arrays
	 * @param left The first array to merge
	 * @param right The second array to merge
	 * @return A merged array
	 */
	public E[] merge(E[] left, E[] right){


		int total = left.length + right.length;
		E[] toReturn = (E[]) new Comparable[total];

		int index=0;
		int lIndex=0;
		int rIndex=0;

		while(lIndex < left.length && rIndex < right.length){

			if(left[lIndex].compareTo(right[rIndex])<=0){
				toReturn[index] = left[lIndex];
				index++;
				lIndex++;				
			}else{
				toReturn[index] = right[rIndex];
				index++;
				rIndex++;				
			}
		}

		//If either of the arrays 'runs out'
		while(lIndex<left.length){
			toReturn[index] = left[lIndex];
			index++;
			lIndex++;	
		}

		while(rIndex<right.length){
			toReturn[index] = right[rIndex];
			index++;
			rIndex++;	
		}

		return toReturn;
	}

}