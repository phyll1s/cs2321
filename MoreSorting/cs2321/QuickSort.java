package cs2321;

/**
 * An inplace quick sort built with an array
 * @author AJMARKIE - Andrew Markiewicz
 *
 * @param <E> Generic type to sort
 */
@SpaceComplexity ("O(n)")
//SCJ The sort is inplace
public class QuickSort<E extends Comparable> implements Sorter<E> {

	/**
	 * Sorts an array using a quicksort
	 */
	@TimeComplexity ("O(n^2)")
	@TimeComplexityExpected ("O(n*log(n)")
	//TCJ The worst case - if the data is sorted it can take O(n^2)
	//TCEJ With random data and if the pivot value is roughly in the middle of the array values, O(n*log(n)) can be achieved
	public void sort(E[] array) {
		sorts(array,0,array.length-1);
	}

	/**
	 * Recursively sorts an array by a quicksort
	 * @param array The array to sort
	 * @param leftIndex The index to start sorting at
	 * @param rightIndex The index to stop sorting at
	 * @return Returns the sorted array
	 */
	@TimeComplexity ("O(n^2)")
	@TimeComplexityExpected ("O(n*log(n)")
	@SpaceComplexity ("O(n)")
	//TCJ The worst case - if the data is sorted it can take O(n^2)
	//TCEJ With random data and if the pivot value is roughly in the middle of the array values, O(n*log(n)) can be achieved
	//SCJ It is in-place
	private void sorts(E[] array, int leftIndex, int rightIndex) {
		//The base case - when the portion of the index is only 1
		if(leftIndex >= rightIndex)
			return;	
		//Sets the pivot - the element that things are sorted around
		E pivot = array[rightIndex];
		int left = leftIndex;
		int right = rightIndex-1;

		while(left<= right){
			//Finds an element that is greater than the pivot
			while(left<=right && array[left].compareTo(pivot)<=0){
				left++;
			}
			//Finds an element that is less than the pivot 
			while(right>=left && array[right].compareTo(pivot)>=0){
				right--;
			}
			//Swaps the two elements
			if(left<right){
				swap(array, left, right);
			}
		}
		//Puts the pivot in the right place
		swap(array, left, rightIndex);
		//Recursively calls sort on the two sections of the array
		sorts(array, leftIndex, left-1);
		sorts(array, left+1, rightIndex);


	}

	/**
	 * Swaps two elements in an array
	 * @param array The array to swap elements in
	 * @param e1 The first element to swap
	 * @param e2 The second element to swap
	 */
	public void swap(E[] array, int e1, int e2){
		E temp = array[e1];
		array[e1] = array[e2];
		array[e2] = temp;		
	}

}
