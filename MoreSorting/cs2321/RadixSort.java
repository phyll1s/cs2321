package cs2321;

/**
 * A Sorting Algorithm based on a repeated Bucket Sort.
 * 
 * Course: CS2321 Section 01
 * Assignment: #6
 * @author AJMARKIE - Andrew Markiewicz
 */
@SpaceComplexity ("O(n)")
//SCJ Each element only needed to be stored once
public class RadixSort implements Sorter<Integer> {

	/**
	 * Uses a radix sort to sort an array
	 */
	@TimeComplexity ("Od(n+N)")
	//TCJ Will need to be iterrated though the number of digits times (the number of unique digits plus n)
	public void sort(Integer[] array) {
		Integer[] temp = radixSort(array);
		for(int i=0; i<array.length; i++){
			array[i]=temp[i];
		}

	}

	/**
	 * Sorts given array using multiple bucket sorts
	 * @param array The array to sort
	 * @return The sorted array
	 */
	@TimeComplexity ("O(n+N")
	//TCJ Will need to be iterrated though the number of digits plus n
	public Integer[] radixSort(Integer [] array){

		int numDigits = findNumDigits(findMaxValue(array));

		int currentDigit = 0;

		for(int i=0; i<numDigits; i++){

			bucketSort(array, currentDigit);
			currentDigit++;

		}

		return array;
	}


	/**
	 * Preforms a bucket sort on an array with regard to a certain digit
	 * @param array The array to sort
	 * @param currentDigit The digit to sort by
	 * @return The sorted array
	 */
	@TimeComplexity ("O(n+N)")
	//TCJ Sorts by the number of unique digits plus n
	public Integer[] bucketSort(Integer[] array, int currentDigit){

		//Creates array of 'buckets', and initializes them
		LinkedSequence<Integer>[] buckets = new LinkedSequence[10];
		for(int i=0; i<10; i++){
			buckets[i] = new LinkedSequence<Integer>();
		}


		//Prints out each bucket
		for(int i=0; i<10; i++){
			buckets[i] = new LinkedSequence<Integer>();
		}

		//Adds each number to the appropriate bucket
		for(int i=0; i<array.length; i++){
			buckets[getDigit(array[i], currentDigit)].addLast(array[i]);
		}

		//Move the elements from the buckets to the array
		int k = 0;


		for(LinkedSequence bucket: buckets){
			while(!bucket.isEmpty()){
				array[k]=(Integer) bucket.removeFirst();
				k++;
			}
		}


		return array;
	}

	/**
	 * Finds the max value in an array
	 * @param array The array to find the max in
	 * @return The max value from the array
	 */
	@TimeComplexity ("O(n)")
	//TCJ Must iterate through all the elements of the array
	public int findMaxValue(Integer[] array){

		int toReturn=array[0];
		for(int i=0; i<array.length; i++){
			if(array[i]>toReturn)
				toReturn=array[i];			
		}		

		return toReturn;
	}

	/**
	 * Finds the number of digits in an integer
	 * @param number The integer to find # of digits
	 * @return The number of digits
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public int findNumDigits(int number){

		return Integer.toString(number).length();		
	}


	/**
	 * Extract a single digit from an integer.
	 * 
	 * Ex: getDigit(1234, 0) is 4
	 *     getDigit(1234, 1) is 3
	 * 
	 * @author Chris Brown / Bill Siever
	 * @param n - The number to extract a digit from
	 * @param d - The digit to extract. 0 is the "1's" position, 1 is the "10's" position, etc.
	 * @return the extracted digit
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public static int getDigit(int n, int d) {
		if(n<0)
			n=-n;
		switch(d) {
		case 0:
			return n%10;
		case 1:
			return (n/10)%10;
		case 2:
			return (n/100)%10;
		case 3:
			return (n/1000)%10;
		case 4:
			return (n/10000)%10;
		case 5:
			return (n/100000)%10;
		case 6:
			return (n/1000000)%10;
		case 7:
			return (n/10000000)%10;
		case 8:
			return (n/100000000)%10;
		case 9:
			return (n/1000000000)%10;
		default:
			return 0;  
		}	
	}




}
