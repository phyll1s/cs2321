import net.datastructures.*;
import cs2321.*;

/**
 * A test driver for the three PriorityQueues.
 * 
 * Course: CS2321 Section ALL
 * Assignment: #4
 * @author Chris Brown (cdbrown@mtu.edu)
 */
public class Main {
	/**
	 * Simple test driver. For Assignment 5, it will need
	 * to be changed to include the PQSort algorithm.
	 * 
	 * @param args unused
	 */
	public static void main(String [] args) {
		PriorityQueue<Integer,String> unordpq = new UnorderedPQ<Integer,String>();
		PriorityQueue<String, Integer> ordpq = new OrderedPQ<String, Integer>();
		PriorityQueue<Double, Integer> heap = new Heap<Double, Integer>();
		
		unordpq.insert(16, "Bulbous Bouffant");
		unordpq.insert(6, "Gazebo");
		unordpq.insert(7, "Balooga");
		unordpq.insert(8, "Galoshes");
		unordpq.insert(6, "Eskimo");
		unordpq.insert(7, "Mukluks");
		unordpq.insert(9, "Macadamia");
		
		ordpq.insert("Bulbous Bouffant", 16);
		ordpq.insert("Gazebo", 6);
		ordpq.insert("Balooga", 7);
		ordpq.insert("Galoshes", 8);
		ordpq.insert("Eskimo", 6);
		ordpq.insert("Mukluks", 7);
		ordpq.insert("Macadamia", 9);

		heap.insert(321.2, 977);
		heap.insert(779.59817432, 624);
		heap.insert(818.728, 50);
		heap.insert(917.596352, 216);
		heap.insert(430.0, 547);
		heap.insert(197.6649, 38);
		heap.insert(50.598212865, 965);
		
		Entry e = unordpq.min();
		System.out.println(e.getKey());
		e = ordpq.min();
		System.out.println(e.getKey());
		e = heap.min();
		System.out.println(e.getKey());
		
		for(int i = 0; i < 7; i++){
			e = unordpq.removeMin();
			System.out.println("unord[" + i + "] - Key: " + e.getKey() + ", Value: " + e.getValue());
			e = ordpq.removeMin();
			System.out.println("order[" + i + "] - Key: " + e.getKey() + ", Value: " + e.getValue());
			e = heap.removeMin();
			System.out.println(" heap[" + i + "] - Key: " + e.getKey() + ", Value: " + e.getValue());
		}
	}	
} // End Main
 