package cs2321;

import java.util.Iterator;
import java.util.NoSuchElementException;

import net.datastructures.BTPosition;
import net.datastructures.BinaryTree;
import net.datastructures.BoundaryViolationException;
import net.datastructures.EmptyTreeException;
import net.datastructures.InvalidPositionException;
import net.datastructures.Position;

/**
 * A complete tree based on an array implementation
 * Course: CS2321 Section R01
 * Assignment: #4
 * @author AJMARKIE - Andrew Markiewicz
 * @param <E> The generic type
 */

public class CompleteTree<E> implements BinaryTree<E> {

	//Create size instance variable and array to store elements
	private int size;
	private BTPosition<E>[] array;

	private class Node<E> implements BTPosition<E>{

		//Instance variables for needed references
		E element;
		int index;
		/**
		 * Constructor to make a new Node
		 * @param elem The element to store
		 * @param p Reference to the parent
		 * @param l Reference to the Left 
		 * @param r Reference to the Right
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public Node(E elem){

			element = elem;
			//index = i;
			//parent = p;
			//left = l;
			//right = r;			
		}

		/**
		 * Method to set the element of the node
		 * @param elem The element to set
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public void setElement(E elem){
			element = elem;
		}

		/**
		 * Method to set the parent of the node
		 * @param p The parent to set to
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public void setParent(BTPosition p){
			array[(index - 1) / 2] = p;	
		}

		/**
		 * Method to set the left of the node
		 * @param l The left to set to
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public void setLeft(BTPosition l){
			array[(index * 2) + 1]= l;
		}

		/**
		 * Method to set the right of the node
		 * @param r The right to set to
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public void setRight(BTPosition r){
			array[(index * 2) + 2] = r;
		}

		/**
		 * Method to set the index of the node
		 * @param i The index to sex
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public void setIndex(Integer i){
			index =i;
		}

		/**
		 * Method to get the element of the node
		 * @return The element of the node
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public E getElement(){
			return element;
		}

		/**
		 * Method to get the parent
		 * @return The parent of the node
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public BTPosition getParent(){
			return (BTPosition<E>) array[(index - 1) / 2];
		}

		/**
		 * Method to get the left
		 * @return The left of the node
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public BTPosition getLeft(){
			return (BTPosition<E>) array[(index  *2) +1];
		}

		/**
		 * Method to get the right
		 * @return The right of the node
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public BTPosition getRight(){
			return (BTPosition<E>) array[(index  *2) +2];
		}

		/**
		 * Method to set the element of the node
		 * @param elem The element to set
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public E element() {
			return element;
		}

		/**
		 * Method to get the index of the node
		 * @return The index of the node
		 */
		@TimeComplexity ("O(1")
		//TCJ Only one operation is called
		public Integer getIndex(){
			return index;
		}

		/**
		 * Returns a string of the position's element and index
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public String toString(){
			String toReturn = "[" + index + ", " + element +"]";
			return toReturn;
		}
	}

	public CompleteTree (){		
		array = new BTPosition[0];
		size = 0;
	}

	/**
	 * Returns a boolean. True if the node has a left, false otherwise
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public boolean hasLeft(Position<E> v) throws InvalidPositionException {
		if(!(v instanceof BTPosition))
			throw new InvalidPositionException("Invalid Position");

		return left(v)!=null;
	}

	/**
	 * Returns a boolean. True if the node has a right, false otherwise
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public boolean hasRight(Position<E> v) throws InvalidPositionException {
		if(!(v instanceof BTPosition))
			throw new InvalidPositionException("Invalid Position");

		return right(v)!=null;
	}

	/**
	 * Returns the left child of a position
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public Position<E> left(Position<E> v) throws InvalidPositionException,
	BoundaryViolationException {

		if(!(v instanceof BTPosition))
			throw new InvalidPositionException("Invalid Position");
		Node<E> temp = (Node<E>)v;
		if(temp.getIndex()*2 + 1 > array.length- 1)
			throw new 	BoundaryViolationException("Boundry Violation");
		return array[temp.getIndex()*2 + 1];
	}

	/**
	 *Returns the right child of a position
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public Position<E> right(Position<E> v) throws InvalidPositionException,
	BoundaryViolationException {
		if(!(v instanceof BTPosition))
			throw new InvalidPositionException("Invalid Position");

		Node<E> temp = (Node<E>)v;

		if(temp.getIndex()>array.length-1)
			throw new BoundaryViolationException("Out of bounds");

		if((temp.getIndex()*2 + 2)>size-1)
			return null;
		return array[temp.getIndex()*2 + 2];
	}

	/**
	 * Returns a LinkedSequence (which is an iterable) with the left child first, then the right
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called - no iterating has to be done
	public Iterable<Position<E>> children(Position<E> v)
	throws InvalidPositionException {

		LinkedSequence<Position<E>> sequence = new LinkedSequence<Position<E>>();
		Node<E> temp = (Node<E>)v;
		sequence.addLast(temp.getLeft());
		sequence.addLast(temp.getRight());
		return sequence;
	}

	/**
	 * Returns true is the tree is empty
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public boolean isEmpty() {
		return (size==0);
	}

	/**
	 * Returns a boolean if the position is external or not
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public boolean isExternal(Position<E> v) throws InvalidPositionException {
		if(!(v instanceof BTPosition))
			throw new InvalidPositionException("Invalid Position");
		Node<E> temp = (Node<E>)v;		
		return (array[temp.getIndex()*2+1] == null && array[temp.getIndex()*2+1] == null);
	}

	/**
	 * Returns a boolean if the position is internal or not
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public boolean isInternal(Position<E> v) throws InvalidPositionException {

		if(v == null)
			throw new InvalidPositionException("Invalid Positino");

		Node<E> temp = (Node<E>)v;	

		return (temp.getLeft() != null || temp.getRight() != null);
	}

	/**
	 * Returns a boolean if the position is the root or not
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public boolean isRoot(Position<E> v) throws InvalidPositionException {
		if(!(v instanceof BTPosition))
			throw new InvalidPositionException("Invalid Position");
		Node<E> temp = (Node<E>)v;
		return temp.getIndex()==0;
	}

	/**
	 * Returns an iterator object of the tree
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public Iterator<E> iterator() {
		Itr<E> iterator = new Itr<E>();
		return iterator;
	}

	/**
	 * Returns the parent of a node
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public Position<E> parent(Position<E> v) throws InvalidPositionException,
	BoundaryViolationException {
		if(!(v instanceof BTPosition))
			throw new InvalidPositionException("Invalid Position");

		Node<E> temp = (Node<E>)v;
		if(temp.getIndex()>array.length-1)
			throw new BoundaryViolationException("Out of bounds");
		if(isRoot(temp))
			return null;
		return temp.getParent();
	}

	/**
	 * Returns an iterable collection of all the elements in the tree
	 */
	@TimeComplexity ("O(n)")
	//TCJ The entire tree must be iterated through to add to the LinkedSequence 
	public Iterable<Position<E>> positions() {
		LinkedSequence<Position<E>> toReturn = new LinkedSequence<Position<E>>();
		for(int i=0; i<size; i++){
			toReturn.addLast(array[i]);
		}
		return toReturn;
	}

	/**
	 * Replaces the element in the node, and returns the old element
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public E replace(Position<E> v, E e) throws InvalidPositionException {
		if(!(v instanceof BTPosition))
			throw new InvalidPositionException("Invalid Position");
		Node<E> temp = (Node<E>)v;
		E tempE = temp.getElement();
		temp.setElement(e);
		return tempE;
	}

	/**
	 * Returns the root of the CompleteTree
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public Position<E> root() throws EmptyTreeException {
		if(size==0)
			throw new EmptyTreeException("Empty Tree");
		return array[0];
	}

	/**
	 * Returns the size of the current tree
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public int size() {
		return size;
	}

	/**
	 * Adds a position to the tree
	 * @param elem The element of the new position
	 * @return The position inserted
	 */
	@TimeComplexity ("O(n)")
	@TimeComplexityAmortized ("O(1)")
	//TCJ The worst case time to add a position is O(n) because the array might have to be doubled in . The time averages out to O(1) after many insertions
	public BTPosition<E> add(E elem){

		Node<E> toAdd = new Node<E>(elem);
		size++;
		//Checks if the size of the array needs to be increased and does so if needed
		if(array.length < size*2 +1){
			BTPosition<E>[] temp = new BTPosition[size*2+1];
			for(int i=0; i<array.length; i++){
				temp[i]=array[i];
			}
			array = temp;
		}		
		array[size-1] = toAdd;
		toAdd.setIndex(size-1);
		return toAdd;	
	}

	/**
	 * Removes and returns the position given 
	 * @param p The position to remove
	 * @return The position removed
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public Position<E> remove(Position<E> p){
		Node toRemove = (Node) p;
		Node toChange = (Node) array[size-1];
		array[toRemove.getIndex()] = toChange;
		toChange.setIndex(toRemove.getIndex());
		array[size] = null;
		size--;
		return toRemove;

	}

	/**
	 * Swaps two positions
	 * @param p1 The first position to swap
	 * @param p2 The second position to swap
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because it is using an array, accessing the element at an index is always O(1)
	public void swap(Position<E> p1, Position<E> p2){
		//Creates references to the 2 positions that are nodes
		Node<E> tempP1 = (Node<E>) p1;
		Node<E> tempP2 = (Node<E>) p2;

		//Holds the old indexs of the nodes
		int p1OldIndex = tempP1.getIndex();

		int p2OldIndex = tempP2.getIndex();

		//Sets the index in the array of p2 to p1 and updates p1's index
		array[p2OldIndex] = tempP1;
		tempP1.setIndex(p2OldIndex);

		//Sets the index in the array of p1 to p2 and updates  p2's index
		array[p1OldIndex] = tempP2;
		tempP2.setIndex(p1OldIndex);		
	}

	/**
	 * Returns a string of all the elements 
	 */
	@TimeComplexity ("O(n)")
	//TCJ All the elements of the tree must be traversed through. O(n) because it is in an array
	public String toString(){

		String toReturn="[";

		for(Position<E> e: positions()){
			toReturn+= e; 
			Node temp = (Node)e;

			if(temp.getIndex()!=size-1)
				toReturn+= ", ";
		}
		toReturn +="]";
		return toReturn;
	}

	/**
	 * Iterator class that iterates through every element in a Complete Tree
	 * @author AJMARKIEWICZ - Andrew Markiewicz
	 *
	 * @param <E> Generic Type
	 */
	@SpaceComplexity ( "O(1)")
	//SCJ The method holds and returns one element at a time
	private class Itr<E> implements java.util.Iterator<E>{

		Node current = (Node) array[0];

		@TimeComplexity ("O(1)")
		//TCJ Method only called one operation
		public boolean hasNext() {
			if(current.getIndex()==size-1)
				return false;
			else
				return true;
		}

		@TimeComplexity ("O(1)")
		//TCJ Method only called one operation
		public E next() {
			try{				
				current = (Node) array[current.getIndex()+1];
			} catch(IndexOutOfBoundsException e) {
				return null;
			}
			return (E) current;
		}


		public void remove() {
			throw new UnsupportedOperationException("Did not implement");
		}

	}

	public static void main(String [] args){

		CompleteTree<String> tree = new CompleteTree<String>();

		System.out.println(tree.add("Hi"));
		System.out.println(tree.add("my"));
		System.out.println(tree.add("name"));
		System.out.println(tree.add("is"));
		System.out.println(tree.add("Andrew"));
		System.out.println(tree.add("Markiewicz"));
		System.out.println(tree);
		System.out.println(tree.isEmpty());
		System.out.println("----------");

		System.out.println(tree.root());
		System.out.println(tree.hasRight(tree.root()));
		System.out.println(tree.left(tree.root()));
		System.out.println(tree.right(tree.root()));
		System.out.println("----------");

		System.out.println(tree.left(tree.left(tree.root())));
		System.out.println(tree.right(tree.left(tree.root())));
		System.out.println(tree.left(tree.right(tree.root())));
		System.out.println(tree.right(tree.right(tree.root())));
		System.out.println("----------");

		System.out.println(tree.children(tree.root()));
		System.out.println("----------");

		System.out.println(tree.isExternal(tree.root()));
		System.out.println(tree.isInternal(tree.root()));
		System.out.println(tree.isExternal(tree.left(tree.left(tree.root()))));
		System.out.println(tree.isInternal(tree.left(tree.right(tree.root()))));
		System.out.println("----------");

		System.out.println(tree.size());
		System.out.println("----------");

		System.out.println(tree.parent(tree.root()));
		System.out.println(tree.parent(tree.left(tree.root())));
		System.out.println("----------");

		System.out.println(tree);
		System.out.println(tree.remove(tree.left(tree.root())));
		System.out.println(tree.size());
		System.out.println(tree);
		System.out.println("----------");

		System.out.println(tree.replace(tree.root(),"Yo"));
		System.out.println(tree);
		System.out.println("----------");

		System.out.println(tree.right(tree.left(tree.root())));
		tree.swap(tree.root(), tree.left(tree.left(tree.root())));
		System.out.println(tree);
	}


}
