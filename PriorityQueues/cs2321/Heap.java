package cs2321;

import net.datastructures.*;
/**
 * A heap based on a complete tree. 
 * 
 * Course: CS2321 Section R01
 * Assignment: #4
 * @author AJMARKIE - Andrew Markiewicz
 */

public class Heap<K extends Comparable<K>,V> implements PriorityQueue<K,V> {

	//Creates tree and size instance variable
	CompleteTree<Entry<K,V>> tree;
	private int size;

	public class MyEntry<K,V> implements Entry<K,V>{
		protected K key;
		protected V value;

		public MyEntry(K k, V v){
			key=k;
			value=v;
		}

		/**
		 * Retruens the key
		 */
		@TimeComplexity ("O(1)")
		//TCJ One operation is called
		public K getKey(){
			return key;
		}

		/**
		 * Returns the value
		 */
		@TimeComplexity ("O(1)")
		//TCJ One operation is called
		public V getValue(){
			return value;
		}

		/**
		 * Returns a string of the entry - [key,value]
		 */
		@TimeComplexity ("O(1)")
		//TCJ One operation is called
		public String toString(){
			return "[" + key + ", " + value + "]";
		}
	}

	/**
	 * Constructor the of the heap
	 */
	public Heap() {
		tree = new CompleteTree<Entry<K,V>>();
		size=0;
	}

	/**
	 * Inserts a new entry and returns said entry
	 */
	@TimeComplexity ("O(n log n")
	//TCJ The entire tree must be traversed by the bubbleUp method
	public Entry<K,V> insert(K key, V value) throws InvalidKeyException {
		checkKey(key);
		Entry<K,V> toAdd = new MyEntry<K,V>(key, value);
		bubbleUp(tree.add(toAdd));
		size++;
		return toAdd;
	}

	/**
	 * Returns a boolean if the heap is empty or not
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public boolean isEmpty() {
		return size==0;
	}

	/**
	 * Returns the minimum element in the heap
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is performed
	public Entry<K,V> min() throws EmptyPriorityQueueException {
		if(size==0)
			throw new EmptyPriorityQueueException("Empty Heap");

		//Return the root b/c if it is sorted, the min element of the heap should be the root
		return tree.root().element();
	}

	/**
	 * Removes and returns the minimum element of the heap
	 */
	@TimeComplexity ("O(n log n)")
	//TCJ The entire tree must be traversed to bubble down
	public Entry<K,V> removeMin() throws EmptyPriorityQueueException {
		if(size==0)
			throw new EmptyPriorityQueueException("Empty Heap");

		Entry<K,V> toReturn = tree.remove(tree.root()).element();
		size--;
		if(size>0)
			bubbleDown(tree.root());

		
		return toReturn;
	}

	/**
	 * Returns the current size of the heap
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public int size() {		
		return size;
	}

	/**
	 * Performs heap bubble up on the given entry
	 * @param e Position to bubble up
	 */
	@TimeComplexity ("O(n log n")
	//TCJ Worst case the entire tree must be traversed
	public void bubbleUp(Position<Entry<K,V>> e){

		Position<Entry<K,V>> toAdd = e;
		Position<Entry<K,V>> pointer;

		while(!tree.isRoot(toAdd)){

			pointer = tree.parent(toAdd);
			if(pointer.element().getKey().compareTo(toAdd.element().getKey())<=0) break;
			tree.swap(toAdd,pointer);
			toAdd = pointer;

		}
	}

	/**
	 * Perfotms heap bubbleDown on the given entry
	 * @param e The position to bubble
	 */
	@TimeComplexity ("O(n log n")
	//TCJ Worst case the entire tree must be traversed
	protected void bubbleDown(Position<Entry<K,V>> r) {
		while (tree.isInternal(r)) {
			Position<Entry<K,V>> s;	

			if (!tree.hasRight(r))
				s = tree.left(r);

			else if (tree.left(r).element().getKey().compareTo(tree.right(r).element().getKey()) <=0){
				s = tree.left(r);
			}

			else{				

				s = tree.right(r);
			}

			if (s.element().getKey().compareTo(r.element().getKey()) < 0) {
				tree.swap(r, s);
				r=s;
			}

			else{
				break;
			}


		}
	}


	/**
	 * Checks to see if the key given is valid
	 * @param key The key to check
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public void checkKey(K key){
		try{
			key.compareTo(key);
		} catch(Exception e) {
			throw new InvalidKeyException("Invalid Key");
		}

	}

	/**
	 * Returns a string of the entire heap
	 */
	@TimeComplexity ("O(log n")
	//TCJ The entire tree must be traversed - stored in an array
	public String toString(){
		return tree.toString();
	}

	public static void main(String [] args){

		Heap<Integer, String> heap = new Heap<Integer, String>();

		System.out.println(heap.size());

		System.out.println("Is empty? " + heap.isEmpty());
		System.out.println(heap.insert(0, "Hi"));
		System.out.println("Is empty? " + heap.isEmpty());
		System.out.println(heap.insert(1, "My"));
		System.out.println(heap.insert(2, "Name"));
		System.out.println(heap.insert(3, "is"));
		System.out.println(heap.insert(4, "Andrew"));
		System.out.println(heap.insert(5, "Markiewicz"));
		System.out.println(heap);
		System.out.println(heap.size());
		System.out.println("Is empty? " + heap.isEmpty());
		System.out.println("----------");

		//System.out.println(heap.min());
		System.out.println(heap);
		System.out.println(heap.removeMin());
		System.out.println(heap);



	}
}
