package cs2321;

import net.datastructures.Sequence;

public class InsertionSortTest {

	public LinkedSequence<Integer> makeOrderedTest(Integer num){

		LinkedSequence<Integer> toReturn = new LinkedSequence<Integer>();

		//java.util.Random random = new java.util.Random();

		
		for(int i=0; i<num; i++){
			toReturn.addLast(i);
		}
		return toReturn;
	}

	public double testInsertionSort(LinkedSequence<Integer> s, Integer i){

		PQSort PQ = new PQSort();

		long minTime = i;
		long stopTime;
		System.gc();
		int samples=0;

		long startTime = System.currentTimeMillis();
		do {
			
			PQ.Unordered(s);  // Sort the data
			samples++;        
			stopTime = System.currentTimeMillis();
		} while(stopTime-startTime < minTime);
		double averageTime = (stopTime-startTime)/(1.0*samples);
		return averageTime;
	}

	public static void main (String [] args) {

		InsertionSortTest test = new InsertionSortTest();
		LinkedSequence<Integer> [] testSequences = new LinkedSequence[13];

		testSequences[0] = test.makeOrderedTest(10);
		System.out.println(testSequences[0]);
		testSequences[1] = test.makeOrderedTest(25);
		System.out.println(testSequences[1]);
		testSequences[2] = test.makeOrderedTest(50);
		testSequences[3] = test.makeOrderedTest(100);
		testSequences[4] = test.makeOrderedTest(1000);
		testSequences[5] = test.makeOrderedTest(2000);
		testSequences[6] = test.makeOrderedTest(4000);
		testSequences[7] = test.makeOrderedTest(8000);
		testSequences[8] = test.makeOrderedTest(16000);
		testSequences[9] = test.makeOrderedTest(32000);
		testSequences[10] = test.makeOrderedTest(64000);
		testSequences[11] = test.makeOrderedTest(128000);
		testSequences[12] = test.makeOrderedTest(256000);

		System.out.println();
		System.out.println("Reversed Data");
		System.out.println("______________");
		for(LinkedSequence<Integer> s: testSequences){
			System.out.println(test.testInsertionSort(s,500));
		}




	}
}
