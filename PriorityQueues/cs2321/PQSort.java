package cs2321;

import net.datastructures.*;

public class PQSort<K extends Comparable<K>>{
	@TimeComplexity("O(n^2)")
	public Sequence<K> Unordered(Sequence<K> R){
		Sequence<K> S = new LinkedSequence<K>();

		PriorityQueue<K,K> PQ = new UnorderedPQ<K,K>();

		//O(n)
		for(K k: R){
			PQ.insert(k,k);
		}
		//O(n^2)
		while(!PQ.isEmpty()){
			K k = PQ.removeMin().getValue();
			S.addLast(k);
		}
		return S;
	}
	@TimeComplexity("O(n^2")
	public Sequence<K> Ordered(Sequence<K> R){
		Sequence<K> S = new LinkedSequence<K>();

		PriorityQueue<K,K> PQ = new OrderedPQ<K,K>();
		for(K k: R){
			PQ.insert(k,k);
		}
		while(!PQ.isEmpty()){
			K k = PQ.removeMin().getValue();
			S.addLast(k);
		}
		return S;
	}
	@TimeComplexity("O(n log n)")
	public Sequence<K> Heap(Sequence<K> R){
		Sequence<K> S = new LinkedSequence<K>();

		PriorityQueue<K,K> PQ = new Heap<K,K>();

		for(K k: R){
			PQ.insert(k,k);
		}


		while(!PQ.isEmpty()){
			K k = PQ.removeMin().getValue();
			S.addLast(k);
		}


		return S;
	}

	public static void main (String [] args){

		PQSort<Integer> PQ = new PQSort<Integer>();

		//Random Data Testing

		LinkedSequence<Integer> sequence = new LinkedSequence<Integer>();
		java.util.Random random = new java.util.Random();

		for(int i=0; i<1000; i++){
			sequence.addLast(i);  //random.nextInt(100000)

		}

		
		//UNORDERED with RANDOM DATA
		long minTime = 500;
		long stopTime;
		System.gc();
		int samples=0;
		long startTime = System.currentTimeMillis();
		do {
			PQ.Unordered(sequence);  // Sort the data
			samples++;        
			stopTime = System.currentTimeMillis();
		} while(stopTime-startTime < minTime);
		double averageTime = (stopTime-startTime)/(1.0*samples);
		System.out.println(averageTime);
	
		 

		
		/*
		//ORDERED with RANDOM DATA
		long minTime = 500;
		long stopTime;
		System.gc();
		int samples=0;

		long startTime = System.currentTimeMillis();
		do {
			PQ.Ordered(sequence);  // Sort the data
			samples++;        
			stopTime = System.currentTimeMillis();
		} while(stopTime-startTime < minTime);
		double averageTime = (stopTime-startTime)/(1.0*samples);
		System.out.println(averageTime);
		 */

		/*
		//HEAP with RANDOM DATA
		long minTime = 500;

		System.gc();
		int samples=0;
		long stopTime;
		long startTime = System.currentTimeMillis();
		do {
			PQ.Heap(sequence);  // Sort the data
			samples++;
			stopTime = System.currentTimeMillis();
		} while(stopTime-startTime < minTime);
		double averageTime = ((stopTime-startTime)/(1.0*samples));
		System.out.println(averageTime);
		 */
		




	}
}
