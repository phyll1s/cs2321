package cs2321;

import net.datastructures.*;
/**
 * A PriorityQueue based on an Unordered Sequence. 
 * 
 * Course: CS2321 Section R01
 * Assignment: #4
 * @author AJMARKIE - Andrew Markiewicz
 *
 */

public class UnorderedPQ<K extends Comparable<K>,V> implements PriorityQueue<K,V> {

	private class Node<K extends Comparable<K>,V> implements Entry<K,V>{

		//Creates the Key and Value instance variables
		K key;
		V value;

		/**
		 * Constructor to create a new Entry Object
		 * @param newK The key to create the entry with
		 * @param newV The value the entry will hold
		 * @return 
		 */
		public Node(K newK, V newV){
			key = newK;
			value = newV;			
		}

		/**
		 * Returns the key of the entry
		 * @return The key
		 */
		public K getKey(){
			return key;
		}

		/**
		 * Returns the value of the entry
		 * @return The value
		 */
		public V getValue(){
			return value;
		}

		/**
		 * Sets the key
		 * @param toSet The new key to set
		 */
		public void setKey(K toSet){
			key = toSet;
		}

		/**
		 * Sets the value
		 * @param toSet The new value
		 */
		public void setValue(V toSet){
			value = toSet;
		}

		/**
		 * Returns a string of the entry
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public String toString(){
			String toReturn=("["+key+", "+ value+"]");
			return toReturn;

		}

	}

	//Creates the sequence to use and size variable
	LinkedSequence<Node<K ,V>> sequence = new LinkedSequence<Node<K,V>>();
	private int size = 0;

	/**
	 * Adds and returns a new entry to the PQ
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called, the PQ doens't need to be iterated through
	public Entry<K,V> insert(K key, V value) throws InvalidKeyException {
		if (!(key instanceof Comparable))
			throw new InvalidKeyException("Invalid Key");
		Node<K,V> toAdd = new Node<K,V>(key, value);
		sequence.addLast(toAdd);
		size++;
		return toAdd;
	}

	/**
	 * Returns true if the sequence is empty
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public boolean isEmpty() {
		return (sequence.size()==0);
	}

	/**
	 * Reutns the minimum element without removing it
	 */
	@TimeComplexity("O(n)")	
	//TCJ The method must iterate through the entire sequence
	public Entry<K,V> min() throws EmptyPriorityQueueException {
		if(isEmpty())
			throw new EmptyPriorityQueueException("Empty PQ");

		Position<Node<K, V>> pointer = sequence.first();

		for(Position<Node<K, V>> e: sequence.positions()){
			if(pointer.element().getKey().compareTo(e.element().getKey()) >=0)
				pointer = e;
		}
		return pointer.element();
	}

	/**
	 * Removes and returns the minimum element of the PQ
	 */
	@TimeComplexity("O(n)")
	//TCJ The method calls min() which must iterate through the entire sequence
	public Entry<K,V> removeMin() throws EmptyPriorityQueueException {
		if(sequence.isEmpty())
			throw new EmptyPriorityQueueException("The PQ is empty");

		Position<Node<K, V>> pointer = sequence.first();
		for(Position<Node<K, V>> e: sequence.positions()){
			if(e.element().getKey().compareTo(pointer.element().getKey()) <0)
				pointer = e;
		}
		sequence.remove(pointer);
		size--;
		return pointer.element();
	}

	/**
	 * Returns the size of the sequence
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public int size() {
		return size;
	}

	/**
	 * Returns a string of the entire PQ
	 */
	@TimeComplexity ("O(n)")
	//TCJ The Entire PQ must be iterated through
	public String toString(){
		return sequence.toString();		
	}

}