import net.datastructures.*;
import cs2321.*;

/**
 * A test driver for the three Maps.
 * 
 * Course: CS2321 Section ALL
 * Assignment: #7
 * @author Chris Brown (cdbrown@mtu.edu)
 */
public class Main {
	/**
	 * Simple test driver for Assignment 7.
	 * 
	 * @param args unused
	 */
	public static void main(String [] args) {

		//Map<Integer,String> log = new Logfile<Integer,String>();
		//		Map<Double, Integer> lookup = new LookupTable<Double, Integer>();
				Map<String, Integer> hash = new HashMap<String, Integer>();

//		log.put(16, "Bulbous Bouffant");
//		log.put(6, "Gazebo");
//		log.put(7, "Balooga");
//		log.put(8, "Galoshes");
//		log.put(7, "Mukluks"); //this should replace and return "Gazebo"
//		log.put(9, "Macadamia");
//		System.out.println(log);
//		log.put(9, "ha");
//		System.out.println(log);



		//		lookup.put(321.2, 977);
		//		lookup.put(779.59817432, 624);
		//		lookup.put(818.728, 50);
		//		lookup.put(917.596352, 216);
		//		lookup.put(430.0, 547);
		//		lookup.put(818.728, 97);  //this should replace and return 50
		//		lookup.put(197.6649, 38);
		//		lookup.put(50.598212865, 965);
		//		System.out.println(lookup);

		//		
				hash.put("", 8);  //this hashes to 0
				hash.put("bB", 5); //this and 
				hash.put("aa", 100);      //this and 
				hash.put("c#", 20);               //this have the same result from .hashcode()
				hash.put("I wonder where I hash to.", 5);
				hash.put("My hashcode is HUGE!", 100);  //having the same value is ok.
				hash.put("aa", 3);                      //having the same key replaces and returns 100
				System.out.println(hash);
		//		log.remove(8);
	//	log.remove(9);
		//		log.remove(7);
		//		log.remove(6);
		//		log.remove(16);
		//		log.remove(8); //nothing to remove
		//		log.remove(0); //nothing to remove
	//	System.out.println(log);
		//	

		//		lookup.remove(50.598212865);
		//		lookup.remove(197.6649);
		//		lookup.remove(321.2);
		//		lookup.remove(430.0);
		//		lookup.remove(779.59817432);
		//		lookup.remove(818.728);
		//		lookup.remove(917.596352);
		//		lookup.remove(321.3); //nothing to remove
		//		System.out.println(lookup);
		////		
				hash.remove("bB");
				hash.remove("bB"); //nothing to remove
				System.out.println(hash);
//		System.out.println(log.get(7)); //returns "Balooga"
//		System.out.println(log.get(8)); //nothing there - removed above
//		System.out.println(log.get(9)); //returns "Macadamia"
//		System.out.println(log.get(10));  //nothing there
		//		
		//		System.out.println(lookup.get(112.5)); //nothing
		//		System.out.println(lookup.get(818.728)); //returns 97
		//		System.out.println(lookup.get(430.0));  //returns 547
		//		System.out.println(lookup.get(917.596352)); //nothing
		//		System.out.println(lookup.get(50.598212864)); //nothing
		//
				System.out.println(hash.get("c#")); //returns 20
				System.out.println(hash.get("aa")); //returns 3
				System.out.println(hash.get(""));  //returns 8
				System.out.println(hash.get("Plethora")); //nothing there
		//	
		//		System.out.println(hash.entries());
		//		System.out.println(hash.values());
		//		System.out.println(hash.keys());
		//		System.out.println(hash.size());
		//		System.out.println(hash.isEmpty());

		//		System.out.println(lookup.entries());
		//		System.out.println(lookup.values());
		//	System.out.println(lookup.keys());
		//		System.out.println(lookup.size());
		//		System.out.println(lookup.isEmpty());

//		System.out.println(log.entries());
//		System.out.println(log.values());
//		System.out.println(log.keys());
//		System.out.println(log.size());
//		System.out.println(log.isEmpty());


	} // End main(String [])

} // End Main
