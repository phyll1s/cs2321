package cs2321;

import net.datastructures.Entry;
import net.datastructures.InvalidKeyException;
import net.datastructures.Map;
import java.lang.reflect.*;

/**
 * Hashmap built from an array of logfiles
 * @author AJMARKIE - Andrew Markiewicz	
 *
 * @param <K> Gereric type of the key
 * @param <V> Generic type of the value stored
 */
public class HashMap<K, V> implements Map<K, V> {

	private Logfile<K,V>[] buckets;
	private int hashsize;
	private int size;

	/**
	 * Constructor to create a hash map by a given hashsize
	 * @param hashsize The hashsize to use
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation called
	public HashMap(int hashsize){
		this.hashsize = hashsize; 
		buckets = (Logfile<K,V>[])new Logfile[hashsize];
	}

	/**
	 * Constructor to create a hash map with the default hashsize of 1031
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation called
	public HashMap(){
		this.hashsize = 1031; 
		buckets = (Logfile<K,V>[])new Logfile[hashsize];
	}

	/**
	 * Returns an iterable collection of all the entries in the hash map
	 * @return iterable collection of all the entries in the hash map
	 */
	@TimeComplexity("O(n + N)")
	//TCJ Worst case must go through every bucket and all the elements
	public Iterable<Entry<K, V>> entries() {
		LinkedSequence<Entry<K,V>> sequence = new LinkedSequence<Entry<K,V>>();
		for(int i=0; i<hashsize; i++){
			if(buckets[i]!=null){
				for(Entry<K,V> e: buckets[i].entries()){
					sequence.addLast(e);
				}
			}
		}
		return sequence;
	}

	/**
	 * Gets the value of the key-value pair without removing it
	 * @param key The key of the key-value pair to get the value of
	 * @return The value of the pair to return
	 */
	@TimeComplexity ("O(n)")
	@TimeComplexityExpected ("O(n/N")
	//TCJ Worst case is all the elements in one bucket - O(n)
	//TCEJ The expected time takes n/N time if a good hash function is used and there is ~1 entry / bucket
	public V get(K key) throws InvalidKeyException {
		if(key==null)
			throw new InvalidKeyException("Invalid (null) key");

		int temp = key.hashCode();
		int compressed = temp%hashsize;

		if(buckets[compressed]==null)
			return null;
		
		return buckets[compressed].get(key);
	}

	/**
	 * Returns boolean depending on if the has map is empty or not
	 * @return boolean depending on if the has map is empty or not
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public boolean isEmpty() {
		return size==0;
	}

	/**
	 * Returns an iterable collection of all the keys in the hash map
	 * @return an iterable collection of all the keys in the hash map
	 */
	@TimeComplexity("O(n + N)")
	//TCJ Worst case must go through every bucket and all the elements
	public Iterable<K> keys() {
		LinkedSequence<K> sequence = new LinkedSequence<K>();
		for(int i=0; i<hashsize; i++){
			if(buckets[i]!=null){
				for(K key: buckets[i].keys()){
					sequence.addLast(key);
				}
			}
		}
		return sequence;
	}

	/**
	 * Puts a new value in the hashmap based on the key given
	 * @param key The key to store the value based on
	 * @param value The value to store
	 * @return If the new entry writes over a previous one it returns the previous value, otherwise null
	 */
	@TimeComplexity ("O(n)")
	@TimeComplexityExpected ("O(n/N")
	//TCJ Worst case all the elements in one bucket
	//TCEJ Expected time with a good hash function so not all the data is in one bucket
	public V put(K key, V value) throws InvalidKeyException {
		size++;
		if(key==null)
			throw new InvalidKeyException("Invalid (null) key");

		int temp = key.hashCode();
		int compressed = temp%hashsize;
		V toReturn=null;
		//If the bucket does not exist, create a new one
		if(buckets[compressed]==null){
			buckets[compressed]= new Logfile<K,V>();
			toReturn = buckets[compressed].put(key, value);
		}else{
			toReturn = buckets[compressed].put(key, value);
		}		
		return toReturn;
	}

	/**
	 * Removes an element based on the given key
	 * @param key The key to the key-item pair to remove
	 * @return The value of the key-value pair removed. Null if not in the hashmap
	 */
	@TimeComplexity ("O(n)")
	@TimeComplexityExpected ("O(n/N")
	//TCJ Worst case is all the elements in one bucket - O(n)
	//TCEJ The expected time takes n/N time if a good hash function is used and there is ~1 entry / bucket
	public V remove(K key) throws InvalidKeyException {
		if(key==null)
			throw new InvalidKeyException("Invalid (null) key");

		int temp = key.hashCode();
		int compressed = temp%hashsize;
		V toReturn=null;

		toReturn = buckets[compressed].remove(key);		
		size--;
		return toReturn;
	}

	/**
	 * Returns the size of the hash map
	 * @return The size of the map
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public int size() {
		return size;
	}

	/**
	 * Returns an iterable collection of all the values in the hashmap
	 * @return an iterable collection of all the values in the hashmap
	 */
	@TimeComplexity ("O()")
	//TCJ
	public Iterable<V> values() {
		LinkedSequence<V> sequence = new LinkedSequence<V>();
		for(int i=0; i<hashsize; i++){
			if(buckets[i]!=null){
				for(V value: buckets[i].values()){
					sequence.addLast(value);
				}
			}
		}
		return sequence;
	}

	/**
	 * Returns a string of all the elements in the hashmap
	 * @return A string of all the elements in the hashmap
	 */
	@TimeComplexity ("O(n)")
	//TCJ Must iterate through all the elements of the hashmap
	public String toString(){
		String toReturn ="[";
		for(Entry<K,V> e: entries()){
			toReturn+= "("+ e.getKey() +", " + e.getValue() + ") ";
		}
		toReturn+="]";
		return toReturn;
	}
}
