package cs2321;

import net.datastructures.Entry;

public class MapEntry<K,V> implements Entry<K,V> {
	K key;
	V value;

	public MapEntry(K k, V v){
		key = k;
		value = v;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}
}
