/**
 * A simple playing Card from a standard deck
 * 
 * @author Bill Siever (bsiever@mtu.edu)
 */
public class Card {
	
	/**
	 * Possible values for the Suit of a Card
	 */
	public enum Suit {
		SPADES, HEARTS, CLUBS, DIAMONDS;
		private final static String [] names = {"Spades", "Hearts", "Clubs", "Diamonds"};
		private final static String [] simpleNames = {"S", "H", "C", "D"};

		public String toString() {
			return simpleNames[ordinal()];
		}

		/**
		 * Get the formal name of the suit
		 * @return formal name
		 */
		public String nameString() {
			return names[ordinal()];
		}
	} // End Suit

	/**
	 * Possible values for the value of a Card.
	 */
	public enum Value {
		ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING;

		private final static String [] names = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "Ten", "Jack","Queen", "King"};
		private final static String [] simpleNames = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J","Q", "K"};

		public String toString() {
			return simpleNames[ordinal()];
		}

		/**
		 * Get the formal name of the value
		 * @return formal name
		 */
		public String nameString() {
			return names[ordinal()];
		}
	} // End Value

    // Fields: A card is a suit and a value
	private Suit suit;
	private Value value;

	/**
	 * Construct a card with a specific value and suit
	 * 
	 * @param s suit
	 * @param v value
	 */
	Card(Suit s, Value v) {
		suit = s;
		value = v;
	}

	/**
	 * The two character string representation of a card's value (value, suit).
	 * Example: "AS" is the Ace of Spades, 2S is the Two of Spades, "TS" is 10 of Spades
	 * 
	 * @return Two character string representation of a card as Value Suit
	 */
	public String toString() {
		return value.toString()+suit.toString();
	}

	/**
	 * Formal name of a card (value of suit)
	 * Example: "Ace of Spades", "2 of Spades", "Ten of Spades"
	 * 
	 * @return 	 formal name of a card (value of suit)
	 */
	public String name() {
		return value.nameString() + " of " + suit.nameString();
	}
} // End Card
