import net.datastructures.Deque;
import net.datastructures.Position;
import net.datastructures.Sequence;
import cs2321.*;

import java.util.Random;

/**
 * An implementation of the SuperShuffler(tm) algorithm and test driver.
 * 
 * Course: CS2321 Section ALL
 * Assignment: #3
 * @author Bill Siever (bsiever@mtu.edu)
 */
public class SuperShuffler {

	/**
	 * Shuffle the cards in the given Sequence using the SuperShuffler(tm) technique
	 * 
	 * @param cards
	 */
	@SpaceComplexity("O(1)")
	@TimeComplexity("O(n^2)")
	//SCJ A sequence is being passed in, no new sequence is created
	//TCJ The highest order operation is n^2 because the method traverses through the entire sequence, and for each element it traverses it again
	public static void superShuffler(Sequence<Card> cards) {

		Position<Card> removeCursor=cards.atIndex((int)(cards.size()*1.0/3));
		Position<Card> insertCursor=cards.atIndex((int)(cards.size()*2.0/3));

		// Get a random number generator
		Random randomNum = new Random(0);

		// Do 2*n permutations: 
		//   An insertion and removal cursor will be randomly moved 
		//   (Worst case is moving 17 locations)
		//   If the card following the removal cursor is not in the
		//   same position as the insertion cursor, it will be removed
		//   and inserted after the insertion cursor.
		for(int i=0; i<2*cards.size(); i++) {

			// Move the removal cursor up to 17 locations
			if(randomNum.nextDouble()>0.5) 
				removeCursor = advancePosition(cards, removeCursor, 17);
			else
				removeCursor = advancePosition(cards, removeCursor, -11);

			// Move the insertion cursor up to 13 locations
			if(randomNum.nextDouble()>0.5) 
				insertCursor = advancePosition(cards, insertCursor, 5);
			else
				insertCursor = advancePosition(cards, insertCursor, -13);

			// Get the card after the removal cursor. 
			Position<Card> removalCard = advancePosition(cards, removeCursor, 1);
			// If it isn't the same as the insertion point, remove and insert
			if(removalCard != insertCursor) {
				Card c = cards.remove(removalCard);
				cards.addAfter(insertCursor, c);
			}
		} // End for
	} // End superShuffler(Sequence<Card>)

	/**
	 * Find a position in the given sequence that is a specific displacement
	 * forward or backward from the initial position (with wrap around).
	 * 
	 * @param seq the sequence to advance
	 * @param p the initial position
	 * @param n the distance to advance. >0 is forward, <0 is backward.
	 * @return a position with the given displacement from the initial position
	 */
	@SpaceComplexity("O(1)")
	@TimeComplexity("O(n)")
	//SCJ A sequence is being passed in, no new sequence is created
	//TCJ The method traverses over the (worst case) entire sequence
	public static <E> Position<E> advancePosition(Sequence<E> seq, Position<E> p, int n) {
		if(n>0) {                   // Go forward n positions (with wrap around)
			for(int i=0;i<n;i++) {
				if(p==seq.last())
					p=seq.first();
				else
					p=seq.next(p);
			}
		} else 
			if(n<0) {               // Go backward n positions (with wrap around)
				for(int i=0;i<-n;i++) {
					if(p==seq.first()) 
						p=seq.last();
					else
						p=seq.prev(p);
				}
			}
		return p;
	} // End advancePosition(Sequence<E>, int)

	/**
	 * Create a new card sequence and return it.
	 * 
	 * @return Sequence
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public static Sequence<Card> getNewCardSequence() {
		
		//		return new ArraySequence<Card>();
		return new LinkedSequence<Card>();
	}



	/**
	 * Generate a sequence of cards for French decks.
	 * A French deck has cards with 13 rank values (Ace, 2, 3, 4, 5, 6, 7, 8, 9, Jack, Queen, King)
	 * and 4 suits (Spades, Hearts, Clubs, Diamonds), and no Jokers. A deck consists of the 
	 * each member of the product of these two sets.
	 * 
	 * The returned sequence of cards will consecutive decks.
	 * Cards in a deck will be ordered by suit. 
	 * A typical sequence would look like: 
	 *     [AS, 2S, ..., TS, JS, QS, KS, AH, ..., KH,  AC, ..., KC, AD, ... KD, AS, ... KD, ...]  
	 *      [---------------------------Deck 1 -------------------------------] [ Deck 2 ]
	 * 
	 * @param n The number of decks to generate
	 * @return a sequence of cards from multiple French decks
	 */
	public static Sequence<Card> getFrenchDecks(int n) {
		Sequence<Card> decks = getNewCardSequence();
		for(int i=0;i<n;i++) {
			for(Card.Suit s: Card.Suit.values()) {
				for(Card.Value v: Card.Value.values()) {
					decks.addLast(new Card(s,v));
				}
			}
		}
		return decks;
	}

	/**
	 * Simple test driver. Allocates and shuffls 4 decks.
	 * 
	 * @param args unused
	 */
	public static void main(String [] args) {
		// Test it: Create a sequence of cards

		Sequence<Card> shoe=getFrenchDecks(4);
		System.out.println("Before Shuffling: " + shoe);
		superShuffler(shoe);
		System.out.println("After Shuffling: " + shoe);

	}

} // End SuperShuffler
