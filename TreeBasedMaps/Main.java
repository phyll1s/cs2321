import net.datastructures.*;
import cs2321.*;

/**
 * A test driver for the three Maps.
 * 
 * Course: CS2321 Section ALL
 * Assignment: #7
 * @author Chris Brown (cdbrown@mtu.edu)
 */
public class Main {
	/**
	 * Simple test driver for Assignment 7.
	 * 
	 * @param args unused
	 */
	public static void main(String [] args) {
	
		Map<String, Integer> map_imp = null;
		
		if(map_imp == null){
			try{
				map_imp = new AVLMap<String, Integer>();
			}catch(RuntimeException e){
				map_imp = null;
			}
		}

		if(map_imp == null){
			try{
				map_imp = new RedBlackMap<String, Integer>();
			}catch(RuntimeException e){
				map_imp = null;
			}
		}

		if(map_imp == null){
			try{
				map_imp = new Multiway_2_4_Map<String, Integer>();
			}catch(RuntimeException e){
				map_imp = null;
			}
		}

		if(map_imp == null){
			try{
				map_imp = new Multiway_a_b_Map<String, Integer>(16,31);
			}catch(RuntimeException e){
				map_imp = null;
			}
		}
		
		if(map_imp == null){
			System.err.println("You must implement a Map");
			System.exit(0);
		}
		
		map_imp.put("h", 1);		
		map_imp.put("a", 2);		
		map_imp.put("b", 3);
		map_imp.put("lorem", 5);
		map_imp.put("tempor", 4); // overwritten later
		map_imp.put("ipsum", 7);
		map_imp.put("dolor", 3);
		map_imp.put("sit", 41); // overwritten later
		map_imp.put("amet", 33);
		map_imp.put("ut", 11); // overwritten later
		map_imp.put("consectetur", 9);
		map_imp.put("adipisicing", 6);
		map_imp.put("elit", 84);
		map_imp.put("sed", 11);
		map_imp.put("do", 14);
		map_imp.put("eiusmod", 81);
		map_imp.put("tempor", 54);
		map_imp.put("incididunt", 26);
		map_imp.put("ut", 6);
		map_imp.put("sit", 42);
		map_imp.put("labore", 76);
		map_imp.put("et", 85);
		map_imp.put("dolore", 93);
		map_imp.put("magna", 51);
		map_imp.put("aliqua", 26);
//		
		System.out.println(map_imp.size()); // this should be 21
//		
//		map_imp.remove("Hello");
//		map_imp.remove("World");
//		map_imp.remove("sit");
//		map_imp.remove("do");
//		map_imp.remove("elite"); // not found
//		map_imp.remove("labor"); // not found
//		
		System.out.println(map_imp.size()); // this should be 17
//		
		System.out.println(map_imp.get("ut")); // this should be 6
		System.out.println(map_imp.get("et")); // this should be 85
		System.out.println(map_imp.get("alit")); // this should be 84
		System.out.println(map_imp.get("dolore")); // this should be 93
		System.out.println(map_imp.get("manga")); // this should be 51
		System.out.println(map_imp.get("sit")); // not found
		
	} // End main(String [])
	
} // End Main
 