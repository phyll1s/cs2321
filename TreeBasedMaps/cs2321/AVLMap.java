
package cs2321;

import java.util.Iterator;

import net.datastructures.*;

/**
 * An AVL map implemented in a linked structure. It's highly broken and I am ashamed of the work I did. I underestimated the assignment, procrastinated, and generally was not a good student. 
 * I could come up with all sorts of excuses (exams, etc..) but in the end, I did not complete it and that is all there is. Nothing more to do now.
 * @author AJMARKIE - Andrew Markiewicz
 *
 */
public class AVLMap<K extends Comparable<K>, V> implements Map<K, V> {



	AVLTree<K,V> tree;

	/**
	 * Constructor that creates a new AVLTree
	 */
	public AVLMap() {
		tree = new AVLTree<K,V>();
	}

	public Iterable<Entry<K, V>> entries() {
		// TODO Auto-generated method stub
		return null;
	}

	@TimeComplexity ("O(n)")
	//TCJ The tree must first be traversed down O(n) SO the overal time is O( n). It is worst case O(lg n) because the tree does not balance itself properly
	public V get(K key) throws InvalidKeyException {
		if(tree.get(key)==null){
			return null;
		}else{	
			return tree.get(key).element().getValue();
		}
	}

	/**
	 * Returns a boolean based on if the AVLMap is empty or not
	 * @return A boolean based on if the AVLMap is empty or not
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public boolean isEmpty() {
		return tree.size()==0;
	}

	public Iterable<K> keys() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Method to put in a new key,value pair
	 * @param key The new key
	 * @param value The new value
	 * @return The 
	 */
	@TimeComplexity ("O(n)")
	@SpaceComplexity ("O(lg n)")
	//TCJ The tree must first be traversed down O(n) SO the overal time is O( n). It is worst case O(lg n) because the tree does not balance itself properly
	//SCJ Because the tree must be recursed up once to update heights O(n) space, and once to rotate O(n) space, the overall space is O(n)It is worst case O(lg n) because the tree does not balance itself properly
	public V put(K key, V value) throws InvalidKeyException {
		if(! (key instanceof Comparable))
			throw new InvalidKeyException("key is invalid (not comparable");

		AVLMapEntry toAdd = new AVLMapEntry(key, value); 
		if(tree.root().element()==null){

			tree.addRoot(toAdd);
			return value;
		}
		if(key.compareTo(tree.root().element().getKey())>0){
			tree.insertRight(tree.root, toAdd);
			return value;
		}else{
			tree.insertLeft(tree.root, toAdd);
			return value;
		}

	}

	public V remove(K key) throws InvalidKeyException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Returns the current size of the AVLMap
	 * @return The current size of the AVLMap
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public int size() {
		return tree.size();
	}

	public Iterable<V> values() {
		// TODO Auto-generated method stub
		return null;
	}

	private class AVLMapEntry implements Entry<K,V>{

		K key;
		V value;

		public AVLMapEntry(K k, V v){
			key = k;
			value = v;
		}

		@Override
		public K getKey() {

			return key;
		}

		@Override
		public V getValue() {

			return value;
		}

	}


	/**
	 * An AVL Tree implemented using a linked structure
	 * @author AJMARKIE - Andrew Markiewicz
	 *
	 * @param <K> The key to store values by. Must be comparable
	 * @param <V> The value to store
	 */
	private class AVLTree<K extends Comparable, V> implements LinkTree<Entry<K,V>>{

		private int size = 0;
		private Node root;

		public AVLTree(){
			//Initializes the root
			root = new Node(null, null, null, null);

			//Sets the root's left and right to new nodes w/ empty elements
			Node left = new Node(root, null,null,null);
			Node right = new Node(root, null,null,null);
			root.setLeft(left);
			root.setRight(right);
		}

		/**

		 * Adds a new root to the tree with if the tree isn't empty. 
		 * @exception Throws a NonEmptyTreeException if the tree isn't empty
		 * @return Returns the new root
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public Position<Entry<K, V>> addRoot(Entry<K, V> e) throws NonEmptyTreeException {
			if(size!=0)
				throw new NonEmptyTreeException("The tree is not empty");

			//Sets the roots new element
			root.setElement(e);
			size++;
			return root;
		}

		/**
		 * Not Implemented
		 */
		@TimeComplexity ("O(?)")
		//TCJ Not implemented
		public void attach(Position<Entry<K, V>> v, BinaryTree<Entry<K, V>> T1,	BinaryTree<Entry<K, V>> T2) throws InvalidPositionException {
			///////////UNUSED METHOD\\\\\\\\\\\\\\
		}

		/**
		 * Method to insert an element left of a given element
		 * @param v The element to the left of to insert
		 * @param e The new entry to insert
		 */
		@TimeComplexity ("O(n)")
		@SpaceComplexity ("O(lg n)")
		//TCJ The tree must first be traversed down O(n) SO the overal time is O( n). It is worst case O(lg n) because the tree does not balance itself properly
		//SCJ Because the tree must be recursed up once to update heights O(n) space, and once to rotate O(n) space, the overall space is O(n)It is worst case O(lg n) because the tree does not balance itself properly
		public Position<Entry<K, V>> insertLeft(Position<Entry<K, V>> v, Entry<K, V> e) throws InvalidPositionException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");
			
			//Traverse down until you find an external node
			Node temp = ((Node)v).getLeft();
			if(temp.element()==null){
				//When external node is found, insert new node
				temp.setElement(e);
				//Create sentinal nodes
				Node nL = new Node(temp, null, null, null);
				Node nR = new Node(temp, null, null, null);
				//set sentinal nodes
				temp.setLeft(nL);
				temp.setRight(nR);
				//updateHeight(temp);
				//insertRestructure(temp);
				return temp;
			}

			while(!isExternal(temp)){
				//If you've found a position that is internal, but the same key
				if(e.getKey().compareTo(((Node)v).element().getKey())==0){
					((Node)v).setElement(e);
					return temp;
				}

				//If the new entry is less than the current node, go left
				if(e.getKey().compareTo(((Node)v).element().getKey())<0){
					temp = temp.getLeft();
				}

				//If the new entry is more than the current node, go right
				if(e.getKey().compareTo(((Node)v).element().getKey())>0){
					temp = temp.getRight();
				}
			}

			if(temp.element()!=e)
				size++;
			//When external node is found, insert new node
			temp.setElement(e);
			//Create sentinal nodes
			Node nL = new Node(temp, null, null, null);
			Node nR = new Node(temp, null, null, null);
			//set sentinal nodes
			temp.setLeft(nL);
			temp.setRight(nR);
			//updateHeight(temp);
			//insertRestructure(temp);

			return temp;
		}

		/**
		 * Method to insert an element right of a given element
		 * @param v The element to the right of to insert
		 * @param e The new entry to insert
		 */
		@TimeComplexity ("O(n)")
		@SpaceComplexity ("O(lg n)")
		//TCJ The tree must first be traversed down O(n) SO the overal time is O( n). It is worst case O(lg n) because the tree does not balance itself properly
		//SCJ Because the tree must be recursed up once to update heights O(n) space, and once to rotate O(n) space, the overall space is O(n)It is worst case O(lg n) because the tree does not balance itself properly
		public Position<Entry<K, V>> insertRight(Position<Entry<K, V>> v, Entry<K, V> e) throws InvalidPositionException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");
			
			//Traverse down until you find an external node
			Node temp = ((Node)v).getRight();


			while(temp.element()!=null){
				//If you've found a position that is internal, but the same key
				if(e.getKey().compareTo(((Node)v).element().getKey())==0){
					((Node)v).setElement(e);
					return temp;
				}

				//If the new entry is less than the current node, go left
				if(e.getKey().compareTo(((Node)v).element().getKey())<0){
					temp = temp.getLeft();
				}

				//If the new entry is more than the current node, go right
				if(e.getKey().compareTo(((Node)v).element().getKey())>0){
					temp = temp.getRight();
				}
			}

			//When external node is found, insert new node
			if(temp.element()!=e)
				size++;
			
			temp.setElement(e);
			//Create sentinal nodes
			Node nL = new Node(temp, null, null, null);
			Node nR = new Node(temp, null, null, null);
			//set sentinal nodes
			temp.setLeft(nL);
			temp.setRight(nR);
			//updateHeight(temp);
			//insertRestructure(temp);

			return temp;
		}

		@Override
		public Entry<K, V> remove(Position<Entry<K, V>> v) throws InvalidPositionException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");
			return null;
		}

		/**
		 * Returns boolean based on whether or not the position has a left child
		 * @param v The position to check for a left child
		 * @return Boolean based on whether or not the position has a left child
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public boolean hasLeft(Position<Entry<K, V>> v)	throws InvalidPositionException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");

			//Returns true when the left child's element is not null. Remember sentinal nodes
			return ((Node)v).getLeft().element()!=null;
		}

		/**
		 * Returns boolean based on whether or not the position has a right child
		 * @param v The position to check for a right child
		 * @return Boolean based on whether or not the position has a right child
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public boolean hasRight(Position<Entry<K, V>> v) throws InvalidPositionException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");

			//Returns true when the right child's element is not null. Remember sentinal nodes
			return ((Node)v).getRight().element()!=null;
		}

		/**
		 * Returns the left child of a given position. Can be a sentinal node
		 * @param v The position to get the left child of
		 * @return The left child of the given position
		 */
		@TimeComplexity ("O(1)")
		//TCJ only one operation called
		public Position<Entry<K, V>> left(Position<Entry<K, V>> v) throws InvalidPositionException, BoundaryViolationException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");
			if(((Node)v).getLeft().element()==null && ((Node)v).getRight().element()==null)
				throw new BoundaryViolationException("Boundry violation - both children null");
			return ((Node)v).getLeft();
		}

		/**
		 * Returns the right child of a given position. Can be a sentinel node
		 * @param v The position to get the right child of
		 * @return The right child of the given position. Can be a sentinel node
		 */
		@TimeComplexity ("O(1)")
		//TCJ only one operation called
		public Position<Entry<K, V>> right(Position<Entry<K, V>> v)	throws InvalidPositionException, BoundaryViolationException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");
			if(((Node)v).getLeft().element()==null && ((Node)v).getRight().element()==null)
				throw new BoundaryViolationException("Boundry violation - both children null");
			return ((Node)v).getRight();
		}

		/**
		 * Returns an iterable collection of the children of a node
		 * @param v The position to get the childern of
		 * @return An iterable collection of the children of the node
		 */
		@TimeComplexity ("O(1)")
		//TCJ Always will take a fixed amount of time to enter all the childer b/c there is only at max 2 children
		public Iterable<Position<Entry<K, V>>> children(Position<Entry<K, V>> v) throws InvalidPositionException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");

			LinkedSequence<Position<Entry<K, V>>> toReturn = new LinkedSequence<Position<Entry<K, V>>>();

			if(((Node)v).getLeft().element()!=null)
				toReturn.addLast(((Node)v).getLeft());
			if(((Node)v).getRight().element()!=null)
				toReturn.addLast(((Node)v).getRight());

			return toReturn;
		}

		/**
		 * Returns boolean of whether or not the tree is empty
		 * @return Whether or nto the tree is empty
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public boolean isEmpty() {
			return size==0;
		}

		/**
		 * Returns a boolean based on if the position is an external node or not
		 * @param v The position to check
		 * @return Boolean based on if the position is an external node or not
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public boolean isExternal(Position<Entry<K, V>> v) throws InvalidPositionException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");
			//Remember sentinel nodes


			return (((Node)v).getLeft().element()==null && ((Node)v).getRight().element()==null);
		}

		/**
		 * Returns a boolean based on if the position is an internal node or not
		 * @param v The position to check
		 * @return Boolean based on if the position is an internal node or not
		 */
		public boolean isInternal(Position<Entry<K, V>> v) throws InvalidPositionException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");
			//Remember sentinel nodes
			return (((Node)v).getLeft().element()!=null || ((Node)v).getRight().element()!=null);
		}

		/**
		 * Returns boolean based on if the position is the root
		 * @param v The position to check
		 * @return Boolean based on if the position is the root
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public boolean isRoot(Position<Entry<K, V>> v) throws InvalidPositionException {
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");
			//If the positions parent is null, then it is the root
			return ((Node)v).getParent()==null;
		}

		@Override
		public Iterator<Entry<K, V>> iterator() {
			// TODO Auto-generated method stub
			return null;
		}

		/**
		 * Returns the parent of the node
		 * @exception InvalidPositionException Thrown when position is null
		 * @exception BoundaryViolationException Thrown when the position has no parent
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public Position<Entry<K, V>> parent(Position<Entry<K, V>> v) throws InvalidPositionException, BoundaryViolationException {
			if(((Node)v).getParent()==null)
				throw new BoundaryViolationException("Boundry Violation - no parent");
			if(v==null)
				throw new InvalidPositionException("Invalid (null) position");

			return ((Node)v).getParent();
		}

		@Override
		public Iterable<Position<Entry<K, V>>> positions() {
			// TODO Auto-generated method stub
			return null;
		}

		/**
		 * Replaces the element of a position
		 * @param v The position to replace the element of
		 * @param e The new element
		 * @return The old element of the position
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public Entry<K, V> replace(Position<Entry<K, V>> v, Entry<K, V> e) throws InvalidPositionException {
			Entry<K,V> toReturn	= ((Node)v).element();
			((Node)v).setElement(e);
			return toReturn;
		}

		/**
		 * Returns the root of the tree
		 * @return The root of the tree
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public Position<Entry<K, V>> root() throws EmptyTreeException {
			//if(size==0)
			//	throw new EmptyTreeException("The tree is empty");
			return root;
		}

		/**
		 * Returns the size of the tree
		 * @return The size of the tree
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public int size() {
			return size;
		}

		/**
		 * Method to rotate nodes a,b,c in a left-left case
		 * @param a Bottom most of three
		 * @param b Middle
		 * @param c 'Top'
		 */
		@TimeComplexity ("O(1)")
		//TCJ O(1) b/c there are no loops and essentially "one" operation
		private void lLCase(Node a, Node b, Node c){
			Node adult;
			try{
				adult = (Node) parent(c);				
			}catch(BoundaryViolationException e){
				adult = null;
			}
			//If C was the root, b is new root and rotate accordingly
			if(adult==null){
				root=b;
				b.setParent(null);

				Node t3 = b.getRight();

				b.setRight(c);
				c.setParent(b);

				c.setLeft(t3);
				t3.setParent(c);
			}else{
				//If c wasn't the root, rotate accordingly

				Node t3 = b.getRight();

				b.setParent(adult);
				adult.setLeft(b);

				c.setLeft(t3);
				t3.setParent(c);

				c.setParent(b);
				b.setRight(c);
			}



		}

		/**
		 * Method to rotate nodes a,b,c in a left-right case
		 * @param a c's child to left
		 * @param b A's child to right
		 * @param c 'Top' 
		 */
		@TimeComplexity ("O(1)")
		//TCJ O(1) b/c there are no loops and essentially "one" operation
		private void lRCase(Node a, Node b, Node c){
			Node adult;
			try{
				adult = (Node) parent(c);				
			}catch(BoundaryViolationException e){
				adult = null;
			}
			if(adult==null){
				root=b;
				b.setParent(null);

				Node t2 = b.getLeft();
				Node t3 = b.getRight();

				b.setLeft(a);
				a.setParent(b);

				a.setRight(t2);
				t2.setParent(a);

				b.setRight(c);
				c.setParent(b);

				c.setLeft(t3);
				t3.setParent(c);				
			}else{
				Node t2 = b.getLeft();
				Node t3 = b.getRight();

				adult.setLeft(b);
				b.setParent(adult);

				b.setLeft(a);
				a.setParent(b);

				a.setRight(t2);
				t2.setParent(a);

				b.setRight(c);
				c.setParent(b);

				c.setLeft(t3);
				t3.setParent(c);				
			}
		}

		/**
		 * Method to rotate nodes a,b,c in a right-left case
		 * @param a C's Parent
		 * @param b C's child
		 * @param c A's child
		 */
		@TimeComplexity ("O(1)")
		//TCJ O(1) b/c there are no loops and essentially "one" operation
		private void rLCase(Node a, Node b, Node c){
			Node adult;
			try{
				adult = (Node) parent(c);				
			}catch(BoundaryViolationException e){
				adult = null;
			}
			if(adult==null){
				Node t2 = b.getLeft();
				Node t3 = b.getRight();

				root = b;
				b.setParent(null);

				b.setLeft(a);
				a.setParent(b);

				a.setRight(t2);
				t2.setParent(a);

				b.setRight(c);
				c.setParent(b);

				c.setLeft(t3);
				t3.setParent(c);
			}else{
				Node t2 = b.getLeft();
				Node t3 = b.getRight();

				adult.setRight(b);
				b.setParent(adult);

				b.setLeft(a);
				a.setParent(b);

				a.setRight(t2);
				t2.setParent(a);

				b.setRight(c);
				c.setParent(b);

				c.setLeft(t3);
				t3.setParent(c);
			}
		}

		/**
		 * Method to rotate nodes a,b,c in a right-right case
		 * @param a 'top'
		 * @param b A's child
		 * @param c B's child
		 */
		@TimeComplexity ("O(1)")
		//TCJ O(1) b/c there are no loops and essentially "one" operation
		private void rRCase(Node a, Node b, Node c){
			Node adult;
			try{
				adult = (Node) parent(c);				
			}catch(BoundaryViolationException e){
				adult = null;
			}
			if(adult==null){
				Node t2 = b.getLeft();

				root=b;
				b.setParent(null);

				b.setLeft(a);
				a.setParent(b);

				a.setRight(t2);
				t2.setParent(a);

				b.setRight(c);
				c.setParent(b);
			}else{
				Node t2 = b.getLeft();

				adult.setRight(b);
				b.setParent(adult);

				b.setLeft(a);
				a.setParent(b);

				a.setRight(t2);
				t2.setParent(a);

				b.setRight(c);
				c.setParent(b);




			}
		}

		/**
		 * Method to recursively update the height of a position and the height of all it's ancestors
		 * @param v The position to start at
		 */
		@TimeComplexity("O(lg n)")
		//TCJ The time complexity is O(lg n) b/c the method must recursively call itself all the way up the tree
		private void updateHeight(Position<Entry<K,V>> v){
		
			Node temp = (Node) v;

			//Updates the current positions height
			if(temp.getLeft().getHeight() > temp.getRight().getHeight())
				temp.setHeight(temp.getLeft().getHeight() + 1);
			else
				temp.setHeight(temp.getRight().getHeight() + 1);

			//Recursively calls update height all the way up the tree

			if(temp.getParent()!=null){
				updateHeight(temp.getParent());				
			}
			return;
		}

		private void insertRestructure(Node v){
			
			if(v.getParent()==null)
				return;
			if(Math.abs(v.getParent().getLeft().getHeight() - v.getParent().getRight().getHeight())>1){

				try{
					if(v.getLeft().getHeight()>v.getRight().getHeight()){
						//Left left case
						if(v.getLeft().getLeft().getHeight()>v.getLeft().getRight().getHeight()){
							
							Node a = v.getLeft().getLeft();
							Node b = v.getLeft();
							Node c = v;
							lLCase(a,b,c);
							updateHeight(a);
							updateHeight(c);
							return;
						}
						//Left right case
						if(v.getLeft().getLeft().getHeight()<v.getLeft().getRight().getHeight()){
						
							Node a = v.getLeft();
							Node b = v.getLeft().getRight();
							Node c = v;
							lRCase(a,b,c);
							updateHeight(a);
							updateHeight(c);
							return;
						}
					}
				}catch(NullPointerException e){

				}
				try{
					if(v.getRight().getHeight()>v.getLeft().getHeight()){
						//Right left case
						if(v.getRight().getLeft().getHeight()>v.getRight().getRight().getHeight()){
						
							Node a = v;
							Node b = v.getRight().getLeft();
							Node c = v.getRight();
							rLCase(a,b,c);
							updateHeight(a);
							updateHeight(c);
							return;
						}
						//Right right case
						if(v.getRight().getLeft().getHeight()<v.getRight().getRight().getHeight()){
							
							Node a = v;
							Node b = v.getRight();
							Node c = v.getRight().getRight();
							rRCase(a,b,c);
							updateHeight(a);
							updateHeight(c);
							return;
						}
					}
				}catch (NullPointerException e){

				}

			}else{
				if(v.getParent()!=null)
					insertRestructure(v.getParent());
				else
					return;
			}
		}

		public Node get(K k){
			if (root.element()==null)
				return null;
			else{
				return getHelp(k, root);
			}

		}

		public Node getHelp(K k, Node v){
			if(v.element()==null)
				return null;
			else{
				K curKey = v.element().getKey();
				if(k.compareTo(curKey)<0)
					return getHelp(k, v.getLeft());
				else if (k.compareTo(curKey)>0)
					return getHelp(k, v.getRight());
				return v;
			}
		}

		/**
		 * A node class for an AVL Tree
		 * @author AJMARKIE - Andrew Markiewicz
		 *
		 */
		private class Node implements Position<Entry<K,V>>{

			private Node parent;
			private Node left;
			private Node right;
			private int height=0;

			private Entry<K,V> element;

			/**
			 * Constructor to create a new node with all variables / pointers
			 * @param p The parent
			 * @param l The left child
			 * @param r The right child
			 * @param k The key of the new entry
			 * @param v The value of the new entry
			 */
			@TimeComplexity ("O(1)")
			//TCJ One essential operation called
			public Node (Node p, Node l, Node r, Entry<K,V> e){
				parent = p;
				left = l;
				right = r;

				element = e;
				height = 0;
			}

			/**
			 * Returns the element stored in the position
			 * @return The element stored in the position
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called
			public Entry<K, V> element() {
				return element;
			}

			/**
			 * Sets the element of the position
			 * @param element The new element to set
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called
			public void setElement(Entry<K, V> element) {
				this.element = element;
			}

			/**
			 * Returns the parent of the position
			 * @return The parent of the position
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called
			public Node getParent() {
				return parent;
			}

			/**
			 * Sets teh parent of the position
			 * @param parent The new parent of the position
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called
			public void setParent(Node parent) {
				this.parent = parent;
			}

			/**
			 * Returns the left child of the position
			 * @return The left child of the position
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called
			public Node getLeft() {
				return left;
			}

			/**
			 * Sets the left child of the position
			 * @param left The new left child to set
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called
			public void setLeft(Node left) {
				this.left = left;
			}

			/**
			 * Returns the right child of the position
			 * @return The right child of the position
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called
			public Node getRight() {
				return right;
			}

			/**
			 * Sets the right child of the position
			 * @param right The new right child to set
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called
			public void setRight(Node right) {
				this.right = right;
			}

			/**
			 * Returns the height of the position
			 * @return The height of the position
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called	
			public int getHeight(){
				return height;
			}

			/**
			 * Sets he height of the position
			 * @param x The new height to set
			 */
			@TimeComplexity ("O(1)")
			//TCJ Only one operation called
			public void setHeight(int x){
				height = x;
			}

		}


	}





}
