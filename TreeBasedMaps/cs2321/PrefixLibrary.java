package cs2321;
import net.datastructures.*;

/**
 * HW8
 * @author AJMARKIE - Andrew Markiewicz
 *A prefix library made for B&C Phone Company. Implemented using a hashmap. It is used to return a word matching to a given prefix that: is the shortest that matches, and 
 *the first that was inputed. It is implemented in a way that the least time is used. 
 *
 *Inserting all the words (in the constructor) takes    
 *
 *Calling a word by it's prefix takes
 */
public class PrefixLibrary {

	private Sequence<String> wordlist;
	private Map<String,String> library;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

//		Sequence<String> s = new LinkedSequence<String>();
//		s.addLast(" frighten");
//		s.addLast("afraid");
//		s.addLast("frigid");
//		s.addLast("fright");
//		s.addLast("african");
//		s.addLast("art");
//		s.addLast("artery");
//
//		PrefixLibrary pl = new PrefixLibrary(s);
//		System.out.println(pl.library);
//
//		System.out.println( pl.word("a"));    //should return "art"
//		System.out.println( pl.word("afr") );  //should return "afraid"
//		System.out.println( pl.word("frig") ); //should return "frigid"
//		System.out.println( pl.word("arte") ); //should return "artery"
//		System.out.println( pl.word("cav") );  //should return ""
//		System.out.println( pl.word(" "));

	}

/**
 *Constructor that initilizes the hashmap and adds all the words to it
 * @param s The Sequence to input into the library
 */
	@TimeComplexity ("O(n^2)")
	@TimeComplexityExpected ("O(n*(n/N))")
	//TCJ Using a HashMap for the library, the worst case time would be O(n) also the prefix library must go through all words
	//TCEJ Using a hashmap, the expected time will be O(n*(n/N)) also the prefix library must go through all words
	public PrefixLibrary(Sequence<String> s) {
		wordlist = new LinkedSequence<String>();
		
		
		
		
		
		
		
		//library = new HashMap<String, String>();

		
		
		
		
		
		
		
		
		
		
		//Goes through all the words
		for(String word: s){
			wordlist.addLast(word);
			String finding = "";
			//Goes through the word and adds the prefix adding one letter at a time to the prefix
			for(int i=0; i<word.length(); i++){
				finding+=word.charAt(i);
				String temp = library.get(finding);
				//If the prefix is not in the library, or the new word is shorter than the existing, then it adds it to the library
				if(temp==null || word.length()<temp.length()){
					library.put(finding, word);
				}
			}
		}
	}

	/**
	 * Easy access to the wordlist
	 * 
	 * @return the current wordlist
	 */
	public Sequence<String> wordlist(){
		return wordlist;
	}

	/**
	 * Given the prefix of a word, this method returns the best word choice
	 * from the dictionary. The best choice is the shortest word from the word
	 * list which begins with the given prefix.  If there is a tie, it picks
	 * the word which appears earlier in the word list.
	 * 
	 * If no word matches the prefix, the method should return "" (not null)
	 * 
	 * @param prefix
	 * @return full word
	 */
	@TimeComplexity ("O(n)")
	@TimeComplexityExpected ("O(n/N")
	//TCJ B/c the library is a hashmap, worst case is all the elements in one bucket - O(n)
	//TCEJ B/c the library is a hashmap, the expected time takes n/N time if a good hash function is used and there is ~1 entry / bucket
	public String word(String prefix){
		String toReturn="";

		String temp = library.get(prefix);
		if(temp!=null){
			toReturn =temp;
		}

		return toReturn;
	}

}
