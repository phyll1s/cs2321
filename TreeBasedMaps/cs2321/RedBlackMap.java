/**
 * 
 */
package cs2321;

import net.datastructures.Entry;
import net.datastructures.InvalidKeyException;
import net.datastructures.Map;

/**
 * @author cdbrown
 *
 */
public class RedBlackMap<K extends Comparable<K>, V> implements Map<K, V> {

	/**
	 * 
	 */
	public RedBlackMap() {
        /*# If you create a RedBlackMap remove the following exception*/
		/*# DO NOT submit more than one working tree-based Map */
		throw new RuntimeException();
	}

	/* (non-Javadoc)
	 * @see net.datastructures.Map#entries()
	 */
	public Iterable<Entry<K, V>> entries() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see net.datastructures.Map#get(java.lang.Object)
	 */
	public V get(K key) throws InvalidKeyException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see net.datastructures.Map#isEmpty()
	 */
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see net.datastructures.Map#keys()
	 */
	public Iterable<K> keys() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see net.datastructures.Map#put(java.lang.Object, java.lang.Object)
	 */
	public V put(K key, V value) throws InvalidKeyException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see net.datastructures.Map#remove(java.lang.Object)
	 */
	public V remove(K key) throws InvalidKeyException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see net.datastructures.Map#size()
	 */
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see net.datastructures.Map#values()
	 */
	public Iterable<V> values() {
		// TODO Auto-generated method stub
		return null;
	}

}
