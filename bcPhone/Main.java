import net.datastructures.*;
import cs2321.*;

/**
 * A test driver for the PrefixLibrary.
 * 
 * Course: CS2321 Section ALL
 * Assignment: #8
 * @author Chris Brown (cdbrown@mtu.edu)
 */
public class Main {
	/**
	 * Simple test driver for Assignment 8.
	 * 
	 * @param args unused
	 */
	public static void main(String [] args) {
		// Just delegate to the PrefixLibrary's main
		PrefixLibrary.main(args);
	} // End main(String [])
	
} // End Main
 