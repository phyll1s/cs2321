package cs2321;

import net.datastructures.Entry;
import net.datastructures.InvalidKeyException;
import net.datastructures.Map;
import net.datastructures.Position;

/**
 * A logfile, which is really a map, that is based on a linked sequence.
 * get(), put(), remove(), keys(), and values() have O(n) time complexity
 * entries(), isEmpty(), and size() have O(1) time complexity 
 * @author AJMARKIE - Andrew Markiewicz
 *
 * @param <K> The generic type of the key
 * @param <V> The generic type of the data to store
 */
public class Logfile<K, V> implements Map<K, V> {

	private LinkedSequence<Entry<K,V>> data;
	private int size;

	/**
	 * Constructor to create an empty logfile
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public Logfile(){
		data = new LinkedSequence<Entry<K,V>>();
	}

	/**
	 * Returns an iterable of all the entires in the logfile
	 * @return Returns an iterable of all the entries
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public Iterable<Entry<K, V>> entries() {
		return data;
	}

	/**
	 * Returns the value that corresponds to the key given. 
	 * @param key The key to find
	 * @return Returns the value tthat corresponds to the key given, null if it's not in the list
	 */
	@TimeComplexity ("O(n)")
	//TCJ Must iterate through the entire logfile to find the appropriate key
	public V get(K key) throws InvalidKeyException {
		if(key==null)
			throw new InvalidKeyException("Invalid (null) Key");

		V temp = null;
		for(Entry<K,V> e: data){
			if(e.getKey().equals(key)){
				temp = e.getValue();
				break;
			}
		}
		return temp;
	}

	/**
	 * Returns boolean if logfile is empty or not
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one opeartion is called
	public boolean isEmpty() {
		return (data.size()==0);
	}

	/**
	 * Returns an iterable that contains all the keys in the logfile
	 */
	@TimeComplexity ("O(n)")
	//TCJ All the entries in the logfile must be iterrated through
	public Iterable<K> keys() {
		LinkedSequence<K> toReturn = new LinkedSequence<K>();

		//Adds each key to the sequence to return
		for(Entry<K,V> e: data){
			toReturn.addLast(e.getKey());
		}
		return toReturn;
	}

	/**
	 * Puts a new entry into the logfile, replacing an entry if it has the same key. 
	 * 
	 * @param key The key to look for
	 * @param value The new value to store
	 * 
	 * @return The old value is returned. Null is returned if the entry does not replace a previous one
	 */
	@TimeComplexity ("O(n)")
	//TCJ Worst case must iterate through all the data
	public V put(K key, V value) throws InvalidKeyException {
		if(key==null)
			throw new InvalidKeyException("Invalid (null) key");
		
		MapEntry<K,V> toAdd = new MapEntry<K,V>(key,value);
		V toReturn = null;

		//Checks if the key is already in the logfile, and replaces the value if so
		//Breaks loop and returns the old if it's found
		for(Position<Entry<K, V>> e: data.positions()){
			if(e.element().getKey().equals(key)){
				toReturn = e.element().getValue();
				data.set(e, toAdd);
				return toReturn;
			}
		}

		//If method is still running (which means it the new entry didn't replace an old, add it to the end of the logfile
		data.addLast(toAdd);		
		return toReturn;
	}

	/**
	 * Removes an entry with given key, and returns the value
	 * @param key The key to find
	 * @return The value that corresponded to the key. Null if the key is not in the list
	 */
	@TimeComplexity ("O(n)")
	//TCJ Worst case must iterate through the entire sequence
	public V remove(K key) throws InvalidKeyException {
		if(key==null)
			throw new InvalidKeyException("Invalid (null) key");
		
		V toReturn = null;
		for(Position<Entry<K, V>> e: data.positions()){
			if(e.element().getKey().equals(key)){
				toReturn=e.element().getValue();
				data.remove(e);
			}
		}
		return toReturn;
	}

	/**
	 * Returns the current size of the logfile
	 */
	@TimeComplexity ("O(1)")
	//Only one operation called
	public int size() {
		return data.size();
	}

	/**
	 * Returns an iterable that contains all the values in the logfile
	 */
	@TimeComplexity ("O(n)")
	//TCJ Must iterate through the entire logfile
	public Iterable<V> values() {
		LinkedSequence<V> toReturn = new LinkedSequence<V>();

		//Adds all the values into the sequence to return
		for(Entry<K,V> e: data){
			toReturn.addLast(e.getValue());
		}
		return toReturn;
	}

	/**
	 * Returns a string of all the elements in the logfile
	 * @return A string of all the elements in the logfile
	 */
	@TimeComplexity ("O(n)")
	//TCJ Must iterate through all the elements of the sequence
	public String toString(){
		String toReturn ="[";
		for(Entry e: data){
			toReturn+="(" + e.getKey() + ", " + e.getValue() +")";
		}
		toReturn+="]";
		return toReturn;
	}

}
