import net.datastructures.Edge;
import net.datastructures.Vertex;
import cs2321.*;

/**
 * @author cdbrown
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		LinkedSequence<String> test1 = new LinkedSequence<String>();
		LinkedSequence<String> test2 = new LinkedSequence<String>();
		LinkedSequence<String> test3 = new LinkedSequence<String>();
		LinkedSequence<String> test4 = new LinkedSequence<String>();
		LinkedSequence<String> test5 = new LinkedSequence<String>();
		
		test1.addLast("LOAD 1 0");
		test1.addLast("LOAD 2 0");
		test1.addLast("ADD 3 2 1");
		test1.addLast("SUB 4 3 1");
		test1.addLast("WRITE 0 4");
				
		//prints out your instruction set for test 1 (valid instruction set)
		//Instructions will be in the same order as given
		for(String s: Mr_Cs_SSA_Gen.testAndCompile(test1)){ System.out.println(s);}
				

		test2.addLast("LOAD 1 0");
		test2.addLast("LOAD 2 0");
		test2.addLast("ADD 3 2 1");
		test2.addLast("SUB 4 1 3");
		test2.addLast("WRITE 0 4");
		
		//prints out your instruction set for test 2 (invalid instruction set)
		for(String s: Mr_Cs_SSA_Gen.testAndCompile(test2)){ System.out.println(s);}
		
		test3.addLast("LOAD 1 0");
		test3.addLast("LOAD 2 0");
		test3.addLast("LOAD 3 0");
		test3.addLast("SUB 4 2 3");
		test3.addLast("MULT 5 4 1");
		test3.addLast("ADD 6 3 1");
		test3.addLast("SUB 7 6 5");
		test3.addLast("WRITE 0 7");
		
		
		//prints out your instruction set for test 3 (valid instruction set)
		// "LOAD 2 0" and "LOAD 3 0" will be switched, otherwise the order is the same
		for(String s: Mr_Cs_SSA_Gen.testAndCompile(test3)){ System.out.println(s);}
		
		test4.addLast("LOAD 1 0");
		test4.addLast("LOAD 2 0");
		test4.addLast("LOAD 3 0");
		test4.addLast("SUB 4 3 2");
		test4.addLast("MULT 5 4 1");
		test4.addLast("ADD 6 1 5");
		test4.addLast("SUB 7 5 6");
		test4.addLast("WRITE 0 7");

		//prints out your instruction set for test 4 (invalid instruction set)
		for(String s: Mr_Cs_SSA_Gen.testAndCompile(test4)){ System.out.println(s);}
		
		test5.addLast("LOAD 3 0");
		test5.addLast("LOAD 7 0");
		test5.addLast("ADD 8 0 0");
		test5.addLast("WRITE 419332 8");
		test5.addLast("ADD 13 8 0");
		test5.addLast("WRITE 954211 13");
		test5.addLast("SUB 7 13 7");
		test5.addLast("LOAD 5 954211");
		test5.addLast("SUB 8 5 7");
		test5.addLast("MULT 3 3 5");
		test5.addLast("SUB 15 8 3");
		test5.addLast("LOAD 2 419332");
		test5.addLast("MULT 10 2 8");
	
		
		//prints out your instruction set for test 5 (valid instruction set)
		//This validates that topological ordering is done by using the natural ordering
		// of the values.
		/* ADD 8 0 0
		 * LOAD 7 0
		 * ADD 13 8 0
		 * SUB 7 13 7
		 * WRITE 419332 8
		 * WRITE 954211 13
		 * LOAD 5 954211
		 * LOAD 3 0
		 * MULT 3 3 5
		 * SUB 8 5 7
		 * LOAD 2 419332
		 * MULT 10 2 8
		 * SUB 15 8 3
		 */
		for(String s: Mr_Cs_SSA_Gen.testAndCompile(test5)){ System.out.println(s);}

	}
	private static void testing(){



		AdjListDiGraph<String, Integer> graph = new AdjListDiGraph<String, Integer>();

		Vertex v1 = graph.insertVertex("Hello");
		System.out.println(v1.element());

		Vertex v2 = graph.insertVertex("My");
		System.out.println(v2.element());

		Vertex v3 = graph.insertVertex("Name");
		System.out.println(v3.element());

		Vertex v4 = graph.insertVertex("Is");
		System.out.println(v4.element());

		Vertex v5 = graph.insertVertex("Andrew");
		System.out.println(v5.element());

		Vertex v6 = graph.insertVertex("Markiewicz");
		System.out.println(v6.element());		

		Edge e1 = graph.insertDirectedEdge(v1, v2, 1);		
		System.out.println(e1.element());

		Edge e2 = graph.insertDirectedEdge(v1, v5, 2);		
		System.out.println(e2.element());

		Edge e3 = graph.insertDirectedEdge(v2, v3, 3);		
		System.out.println(e3.element());

		Edge e4 = graph.insertDirectedEdge(v3, v4, 4);		
		System.out.println(e4.element());

		Edge e5 = graph.insertDirectedEdge(v5, v6, 5);		
		System.out.println(e5.element());

		Edge e6 = graph.insertDirectedEdge(v4, v5, 6);		
		System.out.println(e6.element());

		Edge e7 = graph.insertDirectedEdge(v2, v6, 7);		
		System.out.println(e7.element());

		Edge e8 = graph.insertDirectedEdge(v1, v6, 8);
		
		GraphAlgorithms algorithms = new GraphAlgorithms();
		
		System.out.println(algorithms.toposort(graph).toString());

//
//
//		System.out.println("---------1----------");
//		LinkedSequence<Edge<Integer>> edges1 = (LinkedSequence<Edge<Integer>>) graph.edges();
//		for(Edge<Integer> e : edges1){
//			System.out.println(e.element());
//		}
//
//		LinkedSequence<Vertex<String>> vertices1 = (LinkedSequence<Vertex<String>>) graph.vertices();
//		for(Vertex<String> v : vertices1){
//			System.out.println(v.element());
//		}
//
//		System.out.println("---------1.2----------");
//
//		//		System.out.println(graph.removeVertex(v1));		
//		//		System.out.println(graph.removeVertex(v2));
//
//		System.out.println("---------1.5----------");
//		LinkedSequence<Edge<Integer>> edges2 = (LinkedSequence<Edge<Integer>>) graph.edges();
//		for(Edge<Integer> e : edges2){
//			System.out.println(e.element());
//		}
//
//		LinkedSequence<Vertex<String>> vertices2 = (LinkedSequence<Vertex<String>>) graph.vertices();
//		for(Vertex<String> v : vertices2){
//			System.out.println(v.element());
//		}
//
//
//
//
//		System.out.println("--------2-----------");		
//		System.out.println(graph.areAdjacent(v1, v2));//Should be true
//		System.out.println(graph.areAdjacent(v1, v6));
//		System.out.println(graph.areAdjacent(v1, v5)); //Should be true
//		System.out.println(graph.areAdjacent(v1, v4));
//		System.out.println(graph.areAdjacent(v1, v3));
//
//		System.out.println("~~~~~1");
//
//		System.out.println(graph.areAdjacent(v2, v1));//Should be true
//		System.out.println(graph.areAdjacent(v2, v2));
//		System.out.println(graph.areAdjacent(v2, v3));//Should be true
//		System.out.println(graph.areAdjacent(v2, v4));
//		System.out.println(graph.areAdjacent(v2, v5));
//		System.out.println(graph.areAdjacent(v2, v6)); //Should be true
//
//		System.out.println("~~~~~2");
//
//		System.out.println(graph.areAdjacent(v3, v1));
//		System.out.println(graph.areAdjacent(v3, v2));//Should be true
//		System.out.println(graph.areAdjacent(v3, v3));
//		System.out.println(graph.areAdjacent(v3, v4));//Should be true
//		System.out.println(graph.areAdjacent(v3, v5));
//		System.out.println(graph.areAdjacent(v3, v6));
//
//		System.out.println("~~~~~3");
//
//		System.out.println(graph.areAdjacent(v4, v1));
//		System.out.println(graph.areAdjacent(v4, v2));
//		System.out.println(graph.areAdjacent(v4, v3));//Should be true
//		System.out.println(graph.areAdjacent(v4, v4));
//		System.out.println(graph.areAdjacent(v4, v5));//Should be true
//		System.out.println(graph.areAdjacent(v4, v6));
//
//		System.out.println("~~~~~4");
//
//		System.out.println(graph.areAdjacent(v5, v1));//Should be true
//		System.out.println(graph.areAdjacent(v5, v2));
//		System.out.println(graph.areAdjacent(v5, v3));
//		System.out.println(graph.areAdjacent(v5, v4));//Should be true
//		System.out.println(graph.areAdjacent(v5, v5));
//		System.out.println(graph.areAdjacent(v5, v6));//Should be true
//
//		System.out.println("~~~~~5");
//
//		System.out.println(graph.areAdjacent(v6, v1));
//		System.out.println(graph.areAdjacent(v6, v2));//Should be true
//		System.out.println(graph.areAdjacent(v6, v3));
//		System.out.println(graph.areAdjacent(v6, v4));
//		System.out.println(graph.areAdjacent(v6, v5));//Should be true
//		System.out.println(graph.areAdjacent(v6, v6));
//
//		System.out.println("--------3-----------");		
//		Edge<Integer> e8 = graph.insertEdge(v2, v4, 8);
//
//		LinkedSequence<Edge<Integer>> iEdges1 = (LinkedSequence<Edge<Integer>>) graph.incidentEdges(v1);
//		System.out.println("Incident Edges of v1");
//		for(Edge<Integer> e : iEdges1){
//			System.out.println(e.element());
//		}
//
//		LinkedSequence<Edge<Integer>> iEdges2 = (LinkedSequence<Edge<Integer>>) graph.incidentEdges(v2);
//		System.out.println("Incident Edges of v2");
//		for(Edge<Integer> e : iEdges2){
//			System.out.println(e.element());
//		}
//
//		LinkedSequence<Edge<Integer>> iEdges3 = (LinkedSequence<Edge<Integer>>) graph.incidentEdges(v3);
//		System.out.println("Incident Edges of v3");
//		for(Edge<Integer> e : iEdges3){
//			System.out.println(e.element());
//		}
//
//		LinkedSequence<Edge<Integer>> iEdges4 = (LinkedSequence<Edge<Integer>>) graph.incidentEdges(v4);
//		System.out.println("Incident Edges of v4");
//		for(Edge<Integer> e : iEdges4){
//			System.out.println(e.element());
//		}
//
//		LinkedSequence<Edge<Integer>> iEdges5 = (LinkedSequence<Edge<Integer>>) graph.incidentEdges(v5);
//		System.out.println("Incident Edges of v5");
//		for(Edge<Integer> e : iEdges5){
//			System.out.println(e.element());
//		}
//
//		LinkedSequence<Edge<Integer>> iEdges6 = (LinkedSequence<Edge<Integer>>) graph.incidentEdges(v6);
//		System.out.println("Incident Edges of v6");
//		for(Edge<Integer> e : iEdges6){
//			System.out.println(e.element());
//		}
//
//		System.out.println("--------4-----------");
//
//
//
//		System.out.println(e8.element());
//		System.out.println(graph.areAdjacent(v2, v4));
//		System.out.println(graph.opposite(v2, e8).element());
//		System.out.println(graph.opposite(v4, e8).element());
//
//		System.out.println(graph.opposite(v5, e5).element());
//		System.out.println(graph.opposite(v5, e2).element());
//		System.out.println(graph.opposite(v5, e6).element());
//
//		System.out.println(graph.isDirected(e8));
//		System.out.println(graph.isDirected(e7));
//
//		System.out.println(graph.opposite(v3, e3).element());
//		System.out.println(graph.opposite(v3, e4).element());
//
//		System.out.println("--------5-----------");
//
//		Vertex[] eVertices1 = graph.endVertices(e2);
//		System.out.println(eVertices1[0].element());
//		System.out.println(eVertices1[1].element());
//
//		Vertex[] eVertices2 = graph.endVertices(e8);
//		System.out.println(eVertices2[0].element());
//		System.out.println(eVertices2[1].element());
//
//		System.out.println("--------6-----------");
//
//		LinkedSequence<Edge<Integer>> inEdges1 = (LinkedSequence<Edge<Integer>>) graph.inEdges(v2);
//		for(Edge<Integer> e : inEdges1){
//			System.out.println(e.element());
//		}
//
//		LinkedSequence<Edge<Integer>> inEdges2 = (LinkedSequence<Edge<Integer>>) graph.inEdges(v5);
//		for(Edge<Integer> e : inEdges2){
//			System.out.println(e.element());
//		}
//
//		System.out.println("~~~~~");
//
//
//		LinkedSequence<Edge<Integer>> outEdges1 = (LinkedSequence<Edge<Integer>>) graph.outEdges(v2);
//		for(Edge<Integer> e : outEdges1){
//			System.out.println(e.element());
//		}
//
//		LinkedSequence<Edge<Integer>> outEdges2 = (LinkedSequence<Edge<Integer>>) graph.outEdges(v5);
//		for(Edge<Integer> e : outEdges2){
//			System.out.println(e.element());
//		}
//
//
//		System.out.println("--------7-----------");
//
//		System.out.println(graph.numEdges());
//		System.out.println(graph.numVertices());
	}
}