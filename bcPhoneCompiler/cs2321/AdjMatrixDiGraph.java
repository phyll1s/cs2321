package cs2321;

import net.datastructures.DecorablePosition;
import net.datastructures.DiGraph;
import net.datastructures.Edge;
import net.datastructures.InvalidKeyException;
import net.datastructures.InvalidPositionException;
import net.datastructures.Position;
import net.datastructures.Vertex;

public class AdjMatrixDiGraph<V, E> implements DiGraph<V, E> {

	public AdjMatrixDiGraph() {
		/*# If you create an Edge List Directed Graph, remove the following
		 * exception and complete the constructor appropriately.
		 * DO NOT submit more than one working tree-based Map */
		throw new RuntimeException();
	}

	public boolean areAdjacent(Vertex<V> v, Vertex<V> w) {
		// TODO Auto-generated method stub
		return false;
	}

	public Vertex[] endVertices(Edge<E> e) throws InvalidPositionException {
		// TODO Auto-generated method stub
		return null;
	}

	public Iterable<Edge<E>> inEdges(Vertex<V> v) {
		// TODO Auto-generated method stub
		return null;
	}

	public Edge<E> insertDirectedEdge(Vertex<V> v, Vertex<V> w, E o) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isDirected(Edge<E> e) {
		// TODO Auto-generated method stub
		return false;
	}

	public Iterable<Edge<E>> outEdges(Vertex<V> v) {
		// TODO Auto-generated method stub
		return null;
	}

	public Iterable<Edge<E>> edges() {
		// TODO Auto-generated method stub
		return null;
	}

	public Iterable<Edge<E>> incidentEdges(Vertex<V> v)
	throws InvalidPositionException {
		// TODO Auto-generated method stub
		return null;
	}

	public Edge<E> insertEdge(Vertex<V> u, Vertex<V> v, E o)
	throws InvalidPositionException {
		// TODO Auto-generated method stub
		return null;
	}

	public Vertex<V> insertVertex(V o) {
		// TODO Auto-generated method stub
		return null;
	}

	public int numEdges() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int numVertices() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Vertex<V> opposite(Vertex<V> v, Edge<E> e)
	throws InvalidPositionException {
		// TODO Auto-generated method stub
		return null;
	}

	public E removeEdge(Edge<E> e) throws InvalidPositionException {
		// TODO Auto-generated method stub
		return null;
	}

	public V removeVertex(Vertex<V> v) throws InvalidPositionException {
		// TODO Auto-generated method stub
		return null;
	}

	public V replace(Vertex<V> p, V o) throws InvalidPositionException {
		// TODO Auto-generated method stub
		return null;
	}

	public E replace(Edge<E> p, E o) throws InvalidPositionException {
		// TODO Auto-generated method stub
		return null;
	}

	public Iterable<Vertex<V>> vertices() {
		// TODO Auto-generated method stub
		return null;
	}








}
