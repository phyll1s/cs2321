package cs2321;

import net.datastructures.Entry;
import net.datastructures.InvalidKeyException;
import net.datastructures.Map;

/**
 * A lookup table built from a sorted  map. 
 * @author AJMARKIE - Andrew Markiewicz
 * 
 * entries(), keys(), put(), remove(), values(), and toString() are O(n)
 * get() and binSearch() are O(log(n)) * 
 * 
 * @param <K> The generic  type of the key
 * @param <V> The generic type of the value
 */
public class LookupTable<K extends Comparable, V> implements Map<K , V> {

	//Empty array for the data, int for the size, and int for the initial dimension of the array
	private Entry<K,V>[] data;
	private int size;
	private int dim;

	public LookupTable(){
		dim = 100;
		data = (Entry<K,V>[]) new Entry[dim];
		size = 0;
	}

	/**
	 * Returns an iterable of all the entries in the lookup table
	 */
	@TimeComplexity ("O(n)")
	//TCJ All the entries must be iterated through to add to the sequence
	public Iterable<Entry<K, V>> entries() {
		LinkedSequence<Entry<K,V>> sequence = new LinkedSequence<Entry<K,V>>();
		for(int i=0; i<size; i++){
			sequence.addLast(data[i]);
		}
		return sequence;
	}

	/**
	 * Returns the value associated with the given key
	 * @param key The key to look up
	 * @return The value that corresponds to the given key. Returns null if the key is not in the lookup table
	 */
	@TimeComplexity ("O(log(n))")
	//TCJ Binary search takes O(log(n)) time
	public V get(K key) throws InvalidKeyException {
		if(key==null)
			throw new InvalidKeyException("Invalid (null) key");

		int index = binSearch(key);
		if(index==-1)
			return null;
		else
			return data[index].getValue();
	}

	/**
	 * Returns boolean depending on if the lookup table is empty or not
	 * @return Boolean depending on if the lookup table is empty or not
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public boolean isEmpty() {
		return size==0;
	}

	/**
	 * Returns and iterable of all the keys in the lookup table
	 * @return An iterable of all the keys in the lookup table
	 */
	@TimeComplexity ("O(n)")
	//TCJ All the entires must be iterated through
	public Iterable<K> keys() {
		LinkedSequence<K> toReturn = new LinkedSequence<K>();
		for(int i=0; i<size; i++){
			if(data[i]!= null)
				toReturn.addLast(data[i].getKey());
		}
		return toReturn;
	}

	/**
	 * Puts a new value in the lookup table based on a given key, replacing the old one if the key already exists
	 * @param key The key to store the new value by
	 * @param value The new value to store
	 * @return The old value if it was replaced, null otherwise
	 */
	@TimeComplexity ("O(n)")
	//TCJ The worst case everything will have to be iterated through
	public V put(K key, V value) throws InvalidKeyException {

		if(key==null)
			throw new InvalidKeyException("Invalid (null) key");

		V toReturn = null;
		int index=binSearch(key);
		int tempIndex = 0;

		//If the size is zero, add it as the first element
		if(size==0){

			data[0]= new MapEntry<K,V>(key, value);
			size++;
			return toReturn;
		}		

		//If the key is not already in the index, find where to put it. Shift everything down, and then insert
		if(index==-1){

			//Double the length if needed
			if(size+1>=data.length){

				Entry<K,V>[] newArray = (Entry<K,V>[]) new Entry[data.length*2];
				for(int i=0; i<size; i++){
					newArray[i]=data[i];
				}
				data=newArray;						
			}


			//Finds where to put the new entry
			for(int i=0; i<size; i++){
				if(key.compareTo(data[i].getKey())<0){
					tempIndex=i;
					break;
				}
			}
			//If the entry is belongs at the end, add it there and return null
			if(key.compareTo(data[size-1].getKey())>0){
				data[size]= new MapEntry<K,V>(key, value);
				size++;
				return toReturn;
			}

			//Shift everything over
			for(int i=size; i>tempIndex; i--){
				data[i]=data[i-1];				
			}
			data[tempIndex]=new MapEntry<K,V>(key, value);
		}

		//Replace if the key is already in the lookup table
		else{
			toReturn = data[index].getValue();
			data[index]= new MapEntry<K,V>(key, value);
			return toReturn;
		}
		size++;
		return toReturn;
	}

	/**
	 * Removes the key given shifting the rest over
	 * @param key the key to find
	 * @return The value that corresponds to the key . Returns null if key not in lookup table
	 */
	@TimeComplexity("O(n)")
	//TCJ To find the entry with the binary search takes O(log(n)) time, but worst case everything will have to be shifted over
	public V remove(K key) throws InvalidKeyException {
		if(key==null)
			throw new InvalidKeyException("Invalid (null) key");
		int index = binSearch(key);

		//if it is not in the lookup table
		if(index==-1)
			return null;

		V toReturn = data[index].getValue();

		for(int i=index; i<size; i++){
			if(i==size-1)
				data[i]=null;
			else{
				data[i]=data[i+1];
			}
		}
		size--;
		return toReturn;
	}

	/**
	 * Returns the size of the lookup table
	 * @return The size of the lookup table
	 */
	@TimeComplexity("O(1)")
	//TCJ Only one operation is called
	public int size() {
		return size;
	}

	/**
	 * Returns an iterable of the values in the lookup table
	 * @return an iterable collection of the values in the lookup table
	 */
	@TimeComplexity("O(n)")
	//TCJ All the entries must be iterated through
	public Iterable<V> values() {
		LinkedSequence<V> sequence = new LinkedSequence<V>();
		for(int i=0; i<size; i++){
			if(data[i]!=null)
				sequence.addLast(data[i].getValue());
		}
		return sequence;
	}

	/**
	 * Does a binary search for the key provided
	 * @param key The key to search for
	 * @return the index of the key. -1 if it is not found
	 */
	@TimeComplexity ("O(log(n))")
	private int binSearch(K key){

		int low=0;
		int high=size-1;

		while(low <= high){
			int mid = low + ((high - low) /2);
			if(data[mid].getKey().compareTo(key) >0){
				high = mid-1;
			}
			else{
				if(data[mid].getKey().compareTo(key)<0){
					low = mid+1;
				}
				else
					return mid;	
			}			
		}
		return -1;
	}

	/**
	 * Returns a string with all the elements of the lookup table
	 * @return A string with all the elements of the lookup table
	 */
	@TimeComplexity ("O(n)")
	//TCJ Must iterate through all the elements of the array
	public String toString(){
		String toReturn = "[";

		if(size>0){
			for(int i=0; i<size; i++){
				toReturn+="(" + data[i].getKey() + ", " + data[i].getValue()+")";
				if(i==size-1)
					toReturn+="]";
			}
		}else{
			toReturn+="]";
		}

		return toReturn;
	}
}