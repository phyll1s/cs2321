package cs2321;

import net.datastructures.*;
import java.util.TreeMap;
import java.util.Scanner;

/**
 * @author Mr. C.
 *
 */

public class Mr_Cs_SSA_Gen {
	static final int M = 1048576, R = 32;
	static final String W = "WRITE", L = "LOAD";
	/**
	 * An instruction line is one of three forms<BR>
	 * <ul>
	 * <li>"math_op dest_addr arg1_addr arg2_addr"</li>
	 * <li>"LOAD dest_addr mem_loc"</li>
	 * <li>"WRITE mem_loc arg1_addr"</li>
	 * </ul>
	 * 
	 * @param ins the instructions 
	 * 
	 * @return a Directed Graph representing the dependancies
	 */
	@TimeComplexity ("O(n * lg (n))")
	//TCJ The time complexity is n*lg(n) because the loop runs n times and each of the n times a map operation is called O(lg(n)). All the di-graph methods are O(1).
	public static DiGraph<String, String> SSA(Iterable<String> ins){
		boolean t=false;

		//O(1)
		DiGraph<String,String> r=null;
		if(!t)try{r=new EdgeListDiGraph<String,String>();t=true;}
		catch(RuntimeException e){}
		if(!t)try{r=new AdjListDiGraph<String,String>();t=true;}
		catch(RuntimeException e){}
		if(!t)try{r=new AdjMatrixDiGraph<String,String>();t=true;}
		catch(RuntimeException e){}
		if(!t)return null;

		int nxv=0; int[]ar=new int[R];String op;String ma;Scanner p;
		TreeMap<String,Integer> vm = new TreeMap<String,Integer>();
		Vertex<String>[]v_a=new Vertex[M];Vertex<String> v;
		for(String l: ins){
			//O(1)
			v = r.insertVertex(l);
			int da=0,a1=0,a2=0;p=new Scanner(l);op=p.next();
			if(op.equals(W)){ma=p.next();a1=p.nextInt();
				if(!ma.equals("0")){vm.put(ma,nxv);v_a[nxv++%M]=v;}
				if(a1!=0)r.insertDirectedEdge(v_a[ar[a1%R]%M], v, ""+a1);
			}else if(op.equals(L)){da=p.nextInt();ma=p.next();
				if(!ma.equals("0")){
					//O(1)
					r.insertDirectedEdge(v_a[vm.get(ma)%M], v, ""+ma);}
				if(da!=0){ar[da%R]=nxv++;v_a[ar[da%R]%M]=v;}
			}else{da=p.nextInt();a1=p.nextInt();a2=p.nextInt();
				//O(1)
				if(a1!=0)r.insertDirectedEdge(v_a[ar[a1%R]%M], v, ""+a1);
				//O(1)
				if(a2!=0)r.insertDirectedEdge(v_a[ar[a2%R]%M], v_a[ar[a1%R]%M], ""+a2);
				if(da!=0){ar[da%R]=nxv++;v_a[ar[da%R]%M]=v;}
			}
		}
		return r;
	}
	@TimeComplexity ("O(n +m)")
	//TCJ Must first check if it is a dag (m) and then runs a toposort (n + m)
	public static Iterable<String> testAndCompile(Iterable<String> instructions){
		DiGraph<String,String> ssa_graph = SSA(instructions);
		if(GraphAlgorithms.isDAG(ssa_graph)){
			return GraphAlgorithms.toposort(ssa_graph);
		} else {
			System.out.println("Invalid Instruction Set");
			return new LinkedSequence<String>();
		}
	}
	
}
