package cs2321;


import net.datastructures.DecorablePosition;
import net.datastructures.DiGraph;
import net.datastructures.Edge;
import net.datastructures.InvalidPositionException;
import net.datastructures.Position;
import net.datastructures.Vertex;

/**
 * An Adjacency List Directed Graph implemented with a linked structure. Edges and vertices are capable of being decorated
 * @author AJMARKIE - Andrew Markiewicz
 *
 * @param <V> The generic type stored in the vertices
 * @param <E> The generic type stored in the edges
 */
@SpaceComplexity("O(n + m)")
//SCJ A piece of info must be stored for each edge and vertex because they are both positions
public class AdjListDiGraph<V, E> implements DiGraph<V, E> {

	private LinkedSequence<Vertex> vertices;
	private LinkedSequence<Edge> edges;

	/**
	 * Constructor to initialize the vertice and edge lists
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation called
	public AdjListDiGraph() {
		vertices = new LinkedSequence<Vertex>();
		edges = new LinkedSequence<Edge>();

	}

	/**
	 * Checks to see if two edges are adjacent
	 * @param v The first edge
	 * @param w The second edge
	 * @return Boolean based on whether or not the two edges are adjacent
	 */
	@TimeComplexity ("O(min(deg(v), deg(w)))")
	//TCJ The algorithm determines which vertice has less incident edges, and goes through them. This cuts time down to O(min(deg(v), deg(w)))
	public boolean areAdjacent(Vertex<V> v, Vertex<V> w) {
		//Solves self-adjacency problem when there is no loop
		if(v==w){
			for(Edge e : ((AMDVertex)v).getEdges()){
				if(((AMDEdge)e).getDestination()==v && ((AMDEdge)e).getOrigin()==v)
					return true;
				else
					return false;
			}
		}

		//If v has less incident edges go through it's
		if(((AMDVertex)v).getEdges().size()<=((AMDVertex)w).getEdges().size()){		
			//Go through all the incident edges of v, if any have an origin or destination of 'w', then true, otherwise false
			for(Edge e : ((AMDVertex)v).getEdges()){
				if(((AMDEdge)e).getDestination()==w || ((AMDEdge)e).getOrigin()==w)
					return true;
			}
		}else{
			//Else go through w's incident edges
			//Go through all the incident edges of w, if any have an origin or destination of 'v', then true, otherwise false
			for(Edge e : ((AMDVertex)w).getEdges()){
				if(((AMDEdge)e).getDestination()==v || ((AMDEdge)e).getOrigin()==v)
					return true;
			}

		}
		return false;
	}

	/**
	 * Returns an array of the two vertices of an edge. The Vertex[0] is the origin, Vertex[1] is the destination
	 * @param e The edge to get the vertices of 
	 * @return An array of the vertices of an edge
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only on operation is called
	public Vertex[] endVertices(Edge<E> e) throws InvalidPositionException {
		if(e==null)
			throw new InvalidPositionException("Invalid (null) edge");

		Vertex[] toReturn = new Vertex[2];
		toReturn[0] = ((AMDEdge)e).getOrigin();
		toReturn[1] = ((AMDEdge)e).getDestination();
		return toReturn;
	}

	/**
	 * Returns an iterable of all the edges that have a given vertex as their destination
	 * @param v The vertex to check
	 * @return An iterable of all the edges
	 */
	@TimeComplexity ("O(deg(v)")
	//TCJ Must go through all the incident edges of the vertex to find the ones with destination of that given vertex
	public Iterable<Edge<E>> inEdges(Vertex<V> v) {
		LinkedSequence<Edge<E>> toReturn = new LinkedSequence<Edge<E>>();

		//Goes through each incident edge and adds to the list to return if it's origin is the given vertex or it is an undirected edge
		for(Edge<E> e : ((AMDVertex)v).getEdges()){
			if(((AMDEdge)e).getDestination()==v || !((AMDEdge)e).getDirected())
				toReturn.addLast(e);
		}
		return toReturn;
	}

	/**
	 * Adds a directed edge to the graph
	 * @param v Edge's origin
	 * @param w Edge's Destination
	 * @param o Edge's element
	 * @return The new edge
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because all the operations are O(1), the overal insertion should be O(1). This is accomplished by adding to the end of all the linked sequences
	public Edge<E> insertDirectedEdge(Vertex<V> v, Vertex<V> w, E o) {
		AMDEdge newEdge = new AMDEdge(o, v, w, true);
		newEdge.setOriginSR(((AMDVertex)v).addEdge(newEdge));
		newEdge.setDestinationSR(((AMDVertex)w).addEdge(newEdge));
		edges.addLast(newEdge);
		newEdge.setSelfReference(edges.last());
		return newEdge;
	}

	/**
	 * Returns boolean based on if the edge is directed or not
	 * @return True means it's directed, false not
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation called
	public boolean isDirected(Edge<E> e) {
		return ((AMDEdge)e).getDirected();
	}

	/**
	 * Returns an iterable of all the edges with their origin as the given vertex
	 * @param v The vertex to check
	 * @return An iterable of out edges of a vertex
	 */
	@TimeComplexity ("O(deg(v))")
	//TCJ Must go through all the incident edges of v
	public Iterable<Edge<E>> outEdges(Vertex<V> v) {
		LinkedSequence<Edge<E>> toReturn = new LinkedSequence<Edge<E>>();
		//Goes through each incident edge and adds to the list to return if it's origin is the given vertex
		for(Edge<E> e : ((AMDVertex)v).getEdges()){
			if(((AMDEdge)e).getOrigin()==v || !((AMDEdge)e).getDirected())
				toReturn.addLast(e);			
		}
		return toReturn;
	}

	/**
	 * Returns an iterable of all the edges in the graph
	 * @return An iterable of all the edges in the graph
	 */
	@TimeComplexity ("O(m)")
	//TCJ Must go through all the edges (m)
	public Iterable<Edge<E>> edges() {
		LinkedSequence<Edge<E>> toReturn = new LinkedSequence<Edge<E>>();
		//Copies the edges linked sequence and returns it
		for(Edge<E> e: edges){
			toReturn.addLast(e);
		}
		return toReturn;
	}

	/**
	 * Returns an iterable of all the incident edges of a vertex
	 * @param v The vertex to get the incident edges of
	 * @return An iterable of all the incident edges of the vertex
	 */
	@TimeComplexity ("O(deg(v))")
	//TCJ Must go through all the edges of the vertex to add and return them
	public Iterable<Edge<E>> incidentEdges(Vertex<V> v)	throws InvalidPositionException {
		if(v==null)
			throw new InvalidPositionException("Invalid (null) vertex");

		LinkedSequence<Edge<E>> toReturn = new LinkedSequence<Edge<E>>();
		//Adds all the incident edges of a vertex and returns
		for(Edge<E> e: ((AMDVertex)v).getEdges()){
			toReturn.addLast(e);
		}
		return toReturn;		
	}

	/**
	 * Adds an undirected edge to the graph
	 * @param u The first edge
	 * @param v The second edge
	 * @param o The element stored in the edge
	 * @return The edge
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because all the operations are O(1), the overal insertion should be O(1). This is accomplished by adding to the end of all the linked sequences
	public Edge<E> insertEdge(Vertex<V> u, Vertex<V> v, E o) throws InvalidPositionException {
		if(v == null || u == null)
			throw new InvalidPositionException("One of the vertexes is invalid (null)");

		AMDEdge newEdge = new AMDEdge(o, u, v, false);
		newEdge.setOriginSR(((AMDVertex)u).addEdge(newEdge));
		newEdge.setDestinationSR(((AMDVertex)v).addEdge(newEdge));
		edges.addLast(newEdge);
		newEdge.setSelfReference(edges.last());
		return newEdge;
	}

	/**
	 * Inserts a new vertex into the graph
	 * @param o The new vertex's element
	 * @return The new vertex
	 */
	@TimeComplexity ("O(1)")
	//TCJ O(1) TC is met by adding last to the sequences
	public Vertex<V> insertVertex(V o) {
		AMDVertex newVertex = new AMDVertex(o);
		vertices.addLast(newVertex);
		newVertex.setSelfReference(vertices.last());
		return newVertex;
	}

	/**
	 * Returns the total number of edges in the graph
	 * @return The total number of edges in the graph
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation called
	public int numEdges() {		
		return edges.size();

	}

	/**
	 * Returns the total number of verticies in the graph
	 * @return The total number of verticies in the graph
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation called
	public int numVertices() {
		return vertices.size();
	}

	/**
	 * Returns the opposite vertex given an edge and one of it's vertexes
	 * @param v The vertex
	 * @param e The edge connecting
	 * @return The opposite vertex from the given vertex, along the edge
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation is called
	public Vertex<V> opposite(Vertex<V> v, Edge<E> e) throws InvalidPositionException {
		if(v==null)
			throw new InvalidPositionException("Invalid (null) vertex");
		if(((AMDEdge)e).getDestination()!=v && ((AMDEdge)e).getOrigin()!=v)
			throw new InvalidPositionException("This edge is not an incident edge edge of the vertex given");
		if(((AMDEdge)e).getOrigin()==v)
			return ((AMDEdge)e).getDestination();
		else
			return ((AMDEdge)e).getOrigin();

	}

	/**
	 * Removes an edge from the graph
	 * @param e The edge to remove
	 * @return The element stored in the edge
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because each edge has self references to itself through the DS, removals are immediate
	public E removeEdge(Edge<E> e) throws InvalidPositionException {
		if(e==null)
			throw new InvalidPositionException("Invalid (null) edge");
		//Removes from origin and destination sequences
		((AMDVertex)((AMDEdge)e).getOrigin()).removeEdge(((AMDEdge)e).getOriginSR());
		((AMDVertex)((AMDEdge)e).getDestination()).removeEdge(((AMDEdge)e).getDestinationSR());
		//Removes from the edge list
		edges.remove(((AMDEdge)e).getSelfReference());
		//Removes it's self reference
		((AMDEdge)e).setSelfReference(null);
		((AMDEdge)e).setOrigin(null);
		((AMDEdge)e).setDestination(null);
		return ((AMDEdge)e).element();
	}

	/**
	 * Removes a vertex from the graph
	 * @param v The vertex to remove
	 * @return The element the vertex was holding
	 */
	@TimeComplexity ("O(1)")
	//TCJ Because the vertex has all sorts of self references it can be removed in O(1) time
	public V removeVertex(Vertex<V> v) throws InvalidPositionException {
		if(v==null)
			throw new InvalidPositionException("Invalid (null) vertex");
		while(!((AMDVertex)v).getEdges().isEmpty()){
			this.removeEdge((((AMDVertex)v).getEdges().first().element()));

		}
		vertices.remove(((AMDVertex)v).getSelfReference());
		return ((AMDVertex)v).element();
	}


	/**
	 * Replaces the element of a given vertex with a new element and returns the old element 
	 * @param p The vertex
	 * @param o The new element
	 * @return The old element
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation called
	public V replace(Vertex<V> p, V o) throws InvalidPositionException {
		if(p == null)
			throw new InvalidPositionException("Invalid (null) vertex");
		return ((AMDVertex)p).setElement(o);
	}


	/**
	 * Replaces the element of a given edge with a new element and returns the old element 
	 * @param p The edge
	 * @param o The new element
	 * @return The old element
	 */
	@TimeComplexity ("O(1)")
	//TCJ Only one operation called
	public E replace(Edge<E> p, E o) throws InvalidPositionException {
		if(p == null)
			throw new InvalidPositionException("Invalid (null) edge");
		return ((AMDEdge)p).setElement(o);
	}


	/**
	 * Returns an iterable of all the verticies of the graph
	 * @return An iterable of all the verticies of the graph
	 */
	@TimeComplexity ("O(n)")
	//TCJ Must go through all the verticies (n)
	public Iterable<Vertex<V>> vertices() {
		LinkedSequence<Vertex<V>> toReturn = new LinkedSequence<Vertex<V>>();
		//Adds all the vertexes and returns
		for(Vertex<V> v : vertices){
			toReturn.addLast(v);
		}
		return toReturn;
	}


	//------------------------------------------------------------------\\

	/**
	 * A vertex used for an Adjacency List Directed Graph. Supports decorations
	 */
	@SpaceComplexity ("O(1 + m + d)")
	//SCJ Each vertex holds one element, worst case - references to all the edges, and it's decorations
	private class AMDVertex extends Logfile<Object , Object> implements DecorablePosition<V>, Vertex<V>{

		private LinkedSequence<AMDEdge> edges = new LinkedSequence<AMDEdge>();
		private V element;
		private Position<Vertex> selfReference;

		/**
		 * Constructor that initializes a new vertex
		 * @param e The element the vertex stores
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public AMDVertex(V e){
			element = e;
		}

		/**
		 * Adds an edge to the collection of edges to/from this vertex
		 * @param newEdge The new edge to add
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public Position<AMDEdge> addEdge(AMDEdge newEdge){
			edges.addLast(newEdge);		
			return edges.last();
		}

		/**
		 * Removes an edge from the collection of edges to/from this vertex
		 * @param toRemove The edge to remove
		 * @return The removed Edge
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public AMDEdge removeEdge(Position<AMDEdge> toRemove){
			edges.remove((Position<AMDEdge>) toRemove);
			return toRemove.element();			
		}


		/**
		 * Returns the element stored in this vertex
		 * @return The element in this vertex
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public V element() {			
			return element;
		}

		/**
		 * Returns a linked list of the edges connecting to this vertex
		 * @return A linked list of the edges connecting to this vertex
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public LinkedSequence<AMDEdge> getEdges(){
			return edges;
		}

		/**
		 * Sets the element of vertex
		 * @param e The new element
		 * @return The old element
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public V setElement(V e){
			V temp = element;
			element = e;
			return temp;
		}

		/**
		 * Gets the vertex's self reference in the Adjacency List's vertex sequence
		 * @return The vertex's self reference
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public Position<Vertex> getSelfReference(){
			return selfReference;
		}

		/**
		 * Sets the vertex's self reference in the adjacency list's vertex sequence
		 * @param p The self reference position
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public void setSelfReference(Position<Vertex> p){
			selfReference = p;
		}

	}

	//-------------------------------------------------------------------\\

	/**
	 * A edge used for an Adjacency List Directed Graph. Supports decorations
	 */
	@SpaceComplexity ("O(3 + d")
	//SCJ Each edge holds an element, a reference to the next vertex and the previous vertex, and all it's decorations
	private class AMDEdge extends Logfile<Object, Object> implements DecorablePosition<E>, Edge<E>{

		private E element;
		private Vertex origin;
		private Vertex destination;
		private boolean directed;
		private Position<Edge> selfReference;
		private Position<AMDEdge> originSR;
		private Position<AMDEdge> destinationSR;


		/**
		 * Constructor that initializes the edge
		 * @param value The value the edge holds
		 * @param from The vertex that this edge comes from
		 * @param to The vertex that this edge goes to
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation preformed
		public AMDEdge(E value, Vertex from, Vertex to, boolean dir){
			this.element = value;
			origin = from;
			destination = to;
			directed = dir;
		}

		/**
		 * Returns the element stored in the edge
		 * @return The element stored in the edge
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public E element() {
			return element;
		}

		/**
		 * Sets the element
		 * @param newElement The new element
		 * @return The old element
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public E setElement(E newElement){
			E temp = element;
			element = newElement;
			return temp;
		}

		/**
		 * Sets the origin
		 * @param origin The new origin
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public void setOrigin(Vertex origin) {
			this.origin = origin;
		}

		/**
		 * Sets the destination
		 * @param destination The new destination
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called

		public void setDestination(Vertex destination) {
			this.destination = destination;
		}

		/**
		 * Returns the origin of the edge
		 * @return The origin of the edge
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public Vertex getOrigin(){
			return origin;
		}

		/**
		 * Returns the destination of the edge
		 * @return
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation is called
		public Vertex getDestination(){
			return destination;
		}

		/**
		 * Returns a boolean based on if the edge is directed or not
		 * @return A boolean based on if the edge is directed or not
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public boolean getDirected(){
			return directed;
		}

		/**
		 * Sets directed or undirected boolean
		 * @param dir Boolean based on directed edge or not
		 * 		
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public void setDirected(boolean dir){
			directed = dir;
		}

		/**
		 * Gets the edge's self reference of it's position
		 * @return The edge's reference of it's position
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public Position<Edge> getSelfReference() {
			return selfReference;
		}

		/**
		 * Sets the edge's self reference to it's position
		 * @param selfReference The edge's self reference to it's position
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public void setSelfReference(Position<Edge> selfReference) {
			this.selfReference = selfReference;
		}

		/**
		 * Gets the reference to it's position in the origin's sequence of edges
		 * @return reference to it's position in the origin's sequence of edges
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public Position<AMDEdge> getOriginSR() {
			return originSR;
		}

		/**
		 * Sets the reference to it's position in the origin's sequence of edges
		 * @param originSR the reference to it's position in the origin's sequence of edges
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public void setOriginSR(Position<AMDEdge> originSR) {
			this.originSR = originSR;
		}

		/**
		 * Gets the reference to it's position in the destination's sequence of edges
		 * @return reference to it's position in the destination's sequence of edges
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public Position<AMDEdge> getDestinationSR() {
			return destinationSR;
		}

		/**
		 * Sets the reference to it's position in the destination sequence of edges
		 * @param originSR the reference to it's position in the destination sequence of edges
		 */
		@TimeComplexity ("O(1)")
		//TCJ Only one operation called
		public void setDestinationSR(Position<AMDEdge> destinationSR) {
			this.destinationSR = destinationSR;
		}




	}


}
