package cs2321;
import net.datastructures.*;

/**
 * Graph Algorithms. 
 * @author AJMARKIE - Andrew Markiewicz
 *
 */
public class GraphAlgorithms {

	/**
	 * This method runs a directed depth first search to find the vertex
	 * matching the description of toFind, starting from the vertex matching
	 * the description start.
	 * 
	 * @param <V> implied by the graph type passed in through g
	 * @param <E> implied by the graph type passed in through g
	 * @param g the graph to examine
	 * @param start the starting vertex
	 * @param end the ending vertex
	 * @return whether a path exists from start to end.
	 */
	@TimeComplexity ("O(m)")
	//TCJ Every edge can be visited/iterated through. 
	public static <V,E> boolean directedDFS(Graph<V,E> g, Position<V> start, Position<V> end) throws InvalidPositionException{
		if(start == null || end == null)
			throw new InvalidPositionException("Invalid (null) vertex");

		dFSHelper(g, (Vertex) start);
		return ((Vertex)end).get("DFS")=="Visited" || ((Vertex)end).get("DFS")=="Completed";
	}

	/**
	 * "Helper method" for the DFS - done recursively
	 * @param g The graph
	 * @param v The Starting vertex
	 */
	@TimeComplexity ("O(m)")
	//TCJ Every edge can be visited/iterated through. 
	@SuppressWarnings("unchecked")
	private static void dFSHelper(Graph g, Vertex v){
		v.put("DFS", "Visited");
		LinkedSequence<Edge> firstEdges = (LinkedSequence<Edge>) ((AdjListDiGraph)g).outEdges(v);

		for(Edge e: firstEdges ){
			if(e.get("DFS")==null){
				Vertex w = g.opposite(v, e);
				if(w.get("DFS")=="Completed")
					e.put("DFS", "PTC");
				else if(w.get("DFS")==null){
					e.put("DFS", "Discovery");
					dFSHelper(g, w);
				}else{
					e.put("DFS", "Back");
				}
			}
		}
		v.put("DFS", "Completed");
	}

	/**
	 * This method tests to see if the given graph is a Directed Acyclic Graph. Implemented using my DFS
	 * 
	 * @param <V> implied by the graph type passed in through g
	 * @param <E> implied by the graph type passed in through g
	 * @param g the graph to be tested
	 * @return true if and only if the g is Acyclic.
	 */
	@TimeComplexity ("O(m)")
	//TCJ Must run a DFS on the graph (O(m)) and then go through all the edges once (O(m)). Therefore, O(m)
	public static <V,E> boolean isDAG(Graph<V,E> g){
		isDAGHelper(g,(Vertex)((LinkedSequence)g.vertices()).getFirst(),(Vertex)((LinkedSequence)g.vertices()).getFirst());

		for(Edge e : g.edges()){
			if(e.get("DFS")=="Back")
				return false;
		}
		return true;
	}

	/**
	 * "Helper method" for the isDAG method
	 * @param h The graph
	 * @param v1 The vertex to start on
	 * @param v2 The vertex to end on
	 */
	@TimeComplexity ("O(m)")
	//TCJ Even with a graph that has vertices that aren't connected, the total time taken will still be m
	private static void isDAGHelper(Graph h, Vertex v1, Vertex v2){
		directedDFS(h, v1, v2);
		LinkedSequence<Vertex> notTraversed = new LinkedSequence<Vertex>();
		//Goes through and looks to see if any edges weren't visited (graphs that aren't connected)
		for(Vertex v : ((LinkedSequence<Vertex>)h.vertices())){
			if(v.get("DFS")==null){
				notTraversed.addLast(v);
			}			
		}

		//Recursively calls on the vertices that aren't traversed yet
		if(notTraversed.size()>1)
			isDAGHelper(h, notTraversed.getFirst(), notTraversed.getFirst());
	}

	/**
	 * This method returns a topological ordering of the vertices in a
	 * directed acyclic graph (DAG).  If given a choice between two vertices
	 * that might come next, priority should be given to the one which comes
	 * first in the natural ordering of V (based on .compareTo()).
	 * 
	 * @param <V>
	 * @param <E>
	 * @param g - a directed acyclic graph (DAG)
	 * @return vertices in a topological order.
	 */
	@TimeComplexity ("O(n + m)")
	//TCJ The sort goes through all the vertices and all the edges. But only goes through each one once.
	public static <V extends Comparable<V>,E> Iterable<V> toposort(Graph<V,E> g){
		Sequence<V> ret = new LinkedSequence<V>();

		//Will only work on acyclical graphs
		if(!isDAG(g))
			return ret;

		OrderedPQ<V, Vertex<V>> current0s = new OrderedPQ<V, Vertex<V>>();

		for(Vertex<V> v : g.vertices()){
			//Decorates the vertices with their number of in edges
			v.put("Count", ((LinkedSequence)((AdjListDiGraph)g).inEdges(v)).size());

			//If a vertex has 0 in edges, add it to the current 0 in edges list
			if(((LinkedSequence)((AdjListDiGraph)g).inEdges(v)).size() == 0)
				current0s.insert(v.element(), v);
		}

		while(!current0s.isEmpty()){
			Vertex<V> temp = current0s.removeMin().getValue();

			for(Edge e : (LinkedSequence<Edge>)((AdjListDiGraph)g).outEdges(temp)){
				Vertex<V> opposite = g.opposite(temp, e);
				int oldCount = (Integer) opposite.get("Count");
				e.put("Visited", true);
				opposite.put("Count", --oldCount);
				if(oldCount == 0)
					current0s.insert(opposite.element(), opposite);
			}
			ret.addLast(temp.element());
		}


		return ret;
	}


}
